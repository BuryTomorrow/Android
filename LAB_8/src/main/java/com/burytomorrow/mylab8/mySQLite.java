package com.burytomorrow.mylab8;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by BuryTomorrow on 2017/12/18.
 */

public class mySQLite extends SQLiteOpenHelper{
    private static final String DB_NAME="MyDB.db";
    private static final String TABLE_NAME = "MyTable";
    private static final int DB_VERSION = 1;

    public mySQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }
    public mySQLite(Context context){
        super(context,DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        String CREATE_TABLE = "create table " + TABLE_NAME
                + "(_id integer primary key, "
                + "name text, "
                + "birth text, "
                + "gift text)";
        db.execSQL(CREATE_TABLE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){

    }
}
