package com.burytomorrow.midproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

/**
 * Created by CJT on 2017/11/23.
 */

public class Person_add extends AppCompatActivity{
    private String nation,sex,name,place,birth,info,picture ;

    private RadioGroup sex_RG,nation_RG ;

    private EditText name_ET,place_ET,birth_ET,info_ET ;

    private ImageButton picture_IB ;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_add);

        sex_RG = (RadioGroup) findViewById(R.id.sex_RG) ;
        nation_RG = (RadioGroup) findViewById(R.id.nation_RG) ;

        name_ET = (EditText) findViewById(R.id.name_ET);
        place_ET = (EditText) findViewById(R.id.place_ET) ;
        birth_ET = (EditText) findViewById(R.id.birth_ET) ;
        info_ET = (EditText) findViewById(R.id.info_ET) ;

        picture_IB = (ImageButton) findViewById(R.id.picture_IB) ;
        picture_IB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 打开相册
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent,1);
            }
        });



        Button save = (Button) findViewById(R.id.save) ;
        Button cancel = (Button) findViewById(R.id.cancel) ;
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RadioButton sex_RB = (RadioButton)findViewById(sex_RG.getCheckedRadioButtonId()) ;
                sex = sex_RB.getText().toString() ; // 得到选择的sex
                RadioButton nation_RB = (RadioButton)findViewById(nation_RG.getCheckedRadioButtonId()) ;
                nation = nation_RB.getText().toString() ; // 得到选择的nation
                name = name_ET.getText().toString() ; // 得到输入的name
                place = place_ET.getText().toString() ; // 得到输入的place
                birth = birth_ET.getText().toString() ; // 得到输入的birth
                info = info_ET.getText().toString() ;   // 得到输入的info
                // 回传
                Person person = new Person(nation,name,sex,place,birth,info,picture,picture);
                setResult(RESULT_OK,new Intent().putExtra("person_add",person));
                finish();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if ( resultCode==RESULT_OK ){
            Uri uri = data.getData() ;
            picture = uri.toString() ;
            picture_IB.setImageURI(Uri.parse(picture));
        }
    }
}
