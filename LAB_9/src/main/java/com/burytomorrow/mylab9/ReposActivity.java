package com.burytomorrow.mylab9;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by BuryTomorrow on 2017/12/19.
 */

public class ReposActivity extends AppCompatActivity {
    private static ReposService service ;
    private ProgressBar infoBar;
    private ListView listView;
    private SimpleAdapter simpleAdapter;
    private List<Map<String,Object>> repos;
    String user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        user=(String) getIntent().getSerializableExtra("user");
        init();

        service = createRetrofit("https://api.github.com/").create(ReposService.class);
        service.getReops(user)
                .subscribeOn(Schedulers.newThread()) //新建线程进行网络访问
                .observeOn(AndroidSchedulers.mainThread()) //主线程处理请求结果
                .subscribe(new Subscriber<ArrayList<Repos>>(){
                    @Override
                    public final void onCompleted(){ //请求结束时调用的回调函数
                        infoBar.setVisibility(View.GONE);
                    }
                    @Override
                    public void onError(Throwable e){//出现错误使调用的函数
                        infoBar.setVisibility(View.GONE);
                        Toast.makeText(ReposActivity.this,user+e.hashCode()+"请确认搜索的用户存在",Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onNext(ArrayList<Repos> r){ //收到每一次数据时调用的函数
                        for(int i=0;i<r.size();i++){
                            Map<String,Object> temp = new LinkedHashMap<>();
                            temp.put("name",r.get(i).getName());
                            temp.put("language",r.get(i).getLanguage());
                            temp.put("decription",r.get(i).getDescription());
                            repos.add(temp);
                            simpleAdapter.notifyDataSetChanged();
                        }
                    }
                });
    }
    public void init(){
        infoBar = (ProgressBar) findViewById(R.id.info_bar);
        listView = (ListView) findViewById(R.id.listview);
        repos = new ArrayList<>();
        simpleAdapter = new SimpleAdapter(this,repos,R.layout.info_items,
                new String[]{"name","language","decription"},new int[]{R.id.name,R.id.language,R.id.description});
        listView.setAdapter(simpleAdapter);

    }
    public interface ReposService{
        @GET("/users/{user}/repos")
        Observable<ArrayList<Repos>> getReops(@Path("user") String user);
    }
    private static OkHttpClient createOkHttp(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)//连接超时
                .readTimeout(30,TimeUnit.SECONDS)//读超时
                .writeTimeout(10,TimeUnit.SECONDS)//写超时
                .build();
        return okHttpClient;
    }
    private static Retrofit createRetrofit(String baseUrl){
        return new Retrofit.Builder()
                .baseUrl(baseUrl) //设置baseUrl
                .addConverterFactory(GsonConverterFactory.create())//添加GSON Converter
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())//RxJavaCall Adapter
                .client(createOkHttp())//okHttp
                .build();
    }
}
