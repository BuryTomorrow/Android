package com.burytomorrow.midproject;

import java.io.Serializable;

/**
 * Created by BuryTomorrow on 2017/11/19.
 */


public class Person implements Serializable {
    Person(){}
    Person(String nation, String name, String sex, String place, String birth, String info,
           String picture, String picture_info){
        this.nation = nation ;//所属势力
        this.name = name ;//人物姓名
        this.sex = sex ;//人物性别
        this.place = place ;//户籍
        this.birth = birth ;//生死日期
        this.info = info ; // 人物详情信息
        this.picture = picture ;//图片ID
        this.picture_info = picture_info ; // 背景图片ID
        this.forDelete = false;
    }
    public String name,birth,place,nation,sex,info,picture,picture_info ;
    public boolean forDelete;
}