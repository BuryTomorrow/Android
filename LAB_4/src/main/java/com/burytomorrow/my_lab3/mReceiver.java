package com.burytomorrow.my_lab3;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.widget.Toast;

/**
 * Created by BuryTomorrow on 2017/10/29.
 */

public class mReceiver extends BroadcastReceiver {
    private static final String STATICACTION = "com.example.ex4.MyStaticfliter";//静态广播
    private static final String DYNAMICACTION = "com.example.ex4.MyDynamicFliter";//动态广播
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onReceive(Context context, Intent intent){
        if (intent.getAction().equals(STATICACTION)){//接收的是静态广播
            Info info=(Info) intent.getSerializableExtra("Info");
            int image = info.getImage();
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(),image);
            //获取状态通知栏管理
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            //实例化通知栏构造器
            Notification.Builder builder = new Notification.Builder(context);
            //对Builder进行配置
            builder.setContentTitle("新商品热卖")    //通知栏标题
                    .setContentText(info.getName()+"仅售"+info.getPrice()+":")
                    .setTicker("您有一条新消息")//状态栏显示的提示
                    .setLargeIcon(bm)//下拉显示的大图标
                    .setSmallIcon(R.mipmap.shoplist)//系统状态栏显示的小图标
                    .setAutoCancel(true);//可以点击通知栏的删除按钮删除
            //绑定itent，点击图标时能进入商品详情界面
            Intent mIntent = new Intent(context,MoreInfomation.class);
            mIntent.putExtras(intent.getExtras());
            PendingIntent mPendingIntent = PendingIntent.getActivity(context,0,mIntent,PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(mPendingIntent);
            //横幅显示：
            builder.setFullScreenIntent(mPendingIntent,true);
            //绑定Notification ,发生通知请求
            Notification notify = builder.build();
            manager.notify(0,notify);
        }else if (intent.getAction().equals(DYNAMICACTION)){
            Info info=(Info) intent.getSerializableExtra("Info");
            int image = info.getImage();
            Bitmap bm = BitmapFactory.decodeResource(context.getResources(),image);
            //获取状态通知栏管理
            NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            //实例化通知栏构造器
            Notification.Builder builder = new Notification.Builder(context);
            //对Builder进行配置
            builder.setContentTitle("马上下单")    //通知栏标题
                    .setContentText(info.getName()+"已添加到购物车")
                    .setTicker("您有一条新消息")
                    .setLargeIcon(bm)
                    .setSmallIcon(R.mipmap.shoplist)
                    .setAutoCancel(true);
            //绑定itent，点击图标时能进入购物车列表
            Intent mIntent = new Intent(context,MainActivity.class);
            mIntent.putExtras(intent.getExtras());
            PendingIntent mPendingIntent = PendingIntent.getActivity(context,0,mIntent,PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(mPendingIntent);
            //横幅显示：
            builder.setFullScreenIntent(mPendingIntent,true);
            //绑定Notification ,发生通知请求
            Notification notify = builder.build();
            manager.notify(0,notify);
        }
    }
}
