package com.burytomorrow.lab_7;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private LinearLayout ll;
    private EditText new_pw;
    private EditText conf_pw;
    private EditText pw;
    private Button isok;
    private Button clear;
    private SharedPreferences sharedPreferences;
    boolean flag = false;
    private String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inti();
        sharedPreferences=getSharedPreferences("SaveSetting",MODE_PRIVATE);
        password = sharedPreferences.getString("password","no password");
        if(!password.equals("no password")){
            flag=true;
            new_pw.setVisibility(View.GONE);
            conf_pw.setVisibility(View.GONE);
            pw.setVisibility(View.VISIBLE);
        }
        listener();
    }
    public void inti(){
        ll = (LinearLayout) findViewById(R.id.LL);
        new_pw=(EditText) findViewById(R.id.new_pw);
        conf_pw=(EditText) findViewById(R.id.conf_pw);
        pw=(EditText) findViewById(R.id.pw);
        isok = (Button) findViewById(R.id.ok);
        clear = (Button) findViewById(R.id.clear);
    }
    public void listener(){
        //去掉输入法
        ll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                ll.setFocusable(true);
                ll.setFocusableInTouchMode(true);
                ll.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(isok.getWindowToken(), 0);
                return false;
            }
        });
        //OK按钮的触发事件
        isok.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                if(flag){//如果已经设定过密码
                    String s=pw.getText().toString();
                    if(s.equals(password)){//密码正确，启动文件界面
                        Intent intent=new Intent();
                        intent.setClass(MainActivity.this, File_activity.class);
                        startActivity(intent);
                    }else{
                        Toast.makeText(MainActivity.this,"密码不正确",Toast.LENGTH_SHORT).show();
                    }
                }else{//如果没有设定过密码
                    String s1=new_pw.getText().toString();
                    String s2=conf_pw.getText().toString();
                    if(s1.isEmpty()){//密码不能为空
                        Toast.makeText(MainActivity.this,"密码不能为空",Toast.LENGTH_SHORT).show();
                    }else if(s1.equals(s2)){//两个密码相匹配
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString("password",s1);
                        editor.commit();
                        Intent intent=new Intent();
                        intent.setClass(MainActivity.this, File_activity.class);
                        startActivity(intent);
                    }else{//两个密码不匹配
                        Toast.makeText(MainActivity.this,"密码不匹配",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        //clear按钮的触发事件
        clear.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View view){
                new_pw.setText("");
                conf_pw.setText("");
                pw.setText("");
            }
        });
    }
}
