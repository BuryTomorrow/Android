package com.burytomorrow.my_lab3;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.OvershootInLeftAnimator;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private CommonAdapter<Info> mCommonAdapter;
    private List<Info> Infos;
    private List<Info> Buys;
    private List<Map<String,Object>> MyBuy;
    private ListView mlistview;
    private FloatingActionButton fab;
    private boolean flag =false;
    private SimpleAdapter simpleAdapter;
    private ImageView add_image;
    AlertDialog.Builder builder;

    private static final String STATICACTION = "com.example.ex4.MyStaticfliter";//静态广播


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView=(RecyclerView)findViewById(R.id.recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mlistview = (ListView)findViewById(R.id.mlistview);
        fab = (FloatingActionButton) findViewById(R.id.fab);

        initData();//初始化数据

        ShopOrBuy();//悬浮按钮点击

        face_1();//商品列表界面
        face_2();//购物车列表
        face_1_listen();//商品列表的事件处理
        face_2_listen();//购物车列表的事件处理
    }

    //初始化商品列表数据以及购物车列表数据
    public void initData(){
        Infos = new ArrayList<Info>(){{//创建每一项数据的对象并添加到List中
            add(new Info("Enchated Forest","¥ 5.00","作者","Johanna Basford",R.mipmap.enchatedforest));
            add(new Info("Arla Milk","¥ 59.00","产地","德国",R.mipmap.arla));
            add(new Info("Devondale Milk","¥ 79.00","产地","澳大利亚",R.mipmap.devondale));
            add(new Info("Kindle Oasis","¥ 2399.00","版本","8GB",R.mipmap.kindle));
            add(new Info("waitrose 早餐麦片","¥ 179.00","重量","2Kg",R.mipmap.waitrose));
            add(new Info("Mcvitie's 饼干","¥ 14.90","产地","英国",R.mipmap.mcvitie));
            add(new Info("Ferrero Rocher","¥ 132.59","重量","300g",R.mipmap.ferrero));
            add(new Info("Maltesers","¥ 141.43","重量","118g",R.mipmap.maltesers));
            add(new Info("Lindt","¥ 139.43","重量","249g",R.mipmap.lindt));
            add(new Info("Borggreve","¥ 28.90","重量","640g",R.mipmap.borggreve));
        }};
        MyBuy = new ArrayList<>();
        Buys = new ArrayList<Info>(){{
            add(new Info("假的购物车","价格","null","null",1));
        }};
        Map<String,Object> temp = new LinkedHashMap<>();
        temp.put("cycle","*");
        temp.put("name","假的购物车");
        temp.put("price","价格");
        MyBuy.add(temp);
        builder=new AlertDialog.Builder(this);

        //随机推荐一个商品
        Random random = new Random();
        Intent intentBroadcast = new Intent(STATICACTION);
        intentBroadcast.putExtra("Info",Infos.get(random.nextInt(10)));
        sendBroadcast(intentBroadcast);//发送静态广播

        //注册订阅者
        EventBus.getDefault().register(this);
    }

    //悬浮按钮处理两个列表界面
    public void ShopOrBuy(){
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flag == false){  //商品列表
                    flag = true;
                    fab.setImageResource(R.mipmap.mainpage);
                    recyclerView.setVisibility(View.INVISIBLE);
                    mlistview.setVisibility(View.VISIBLE);
                }else{          //购物车列表
                    flag = false;
                    fab.setImageResource(R.mipmap.shoplist);
                    recyclerView.setVisibility(View.VISIBLE);
                    mlistview.setVisibility(View.INVISIBLE);
                }
            }
        });
        //悬浮按钮长按设置
        fab.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                add_newinfos();
                return false;
            }
        });
    }

    //商品列表界面
    public void face_1(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mCommonAdapter = new CommonAdapter<Info>(this,R.layout.shopping_list,Infos){
            @Override
            public void convert(MyViewHolder holder, Info s){
                TextView cycle = holder.getView(R.id.cycle);
                cycle.setText(String.valueOf(s.getcycle()));
                TextView name = holder.getView(R.id.name);
                name.setText(s.getName());
            }
        };

        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(mCommonAdapter);
        animationAdapter.setDuration(1000);
        recyclerView.setAdapter(animationAdapter);
        recyclerView.setItemAnimator(new OvershootInLeftAnimator());
    }
    //购物车列表界面
    public void face_2(){
        simpleAdapter = new SimpleAdapter(this,MyBuy,R.layout.mybuy,
                new String[]{"cycle","name","price"},new int[]{R.id.cycle,R.id.name,R.id.price});
        mlistview.setAdapter(simpleAdapter);
    }

    //商品列表的事件处理
    public void face_1_listen(){
        mCommonAdapter.setOnItemClickListener(new CommonAdapter.OnItemClickListener(){
            @Override
            public void onClick(int position){//点击事件
                Intent intent = new Intent(MainActivity.this,MoreInfomation.class);
                Info temp = Infos.get(position);
                intent.putExtra("Info",temp);
                startActivity(intent);
            }
            @Override
            public void onLongClick(int position){//长按事件
                Infos.remove(position);
                mCommonAdapter.notifyItemRemoved(position);
                Toast.makeText(MainActivity.this,"您竟然删除了第"+position+"个商品！",Toast.LENGTH_SHORT).show();
            }
        });
    }
    //购物车列表的事件处理
    public void face_2_listen(){
        //购物车点击事件
        mlistview.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView,View view,int i,long l){
                if(i!=0){
                    Intent intent = new Intent(MainActivity.this,MoreInfomation.class);
                    Info temp = Buys.get(i);
                    intent.putExtra("Info",temp);
                    startActivity(intent);
                }
            }
        });

        //购物车长按事件
        mlistview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> parent,View view,final int position,long id){
                if(position!=0){
                    final String[] items=new String[]{"从购物车移除"};
                    builder.setTitle("移除商品");
                    builder.setMessage("从购物车中移除"+Buys.get(position).getName()+"?");
                    builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Toast.makeText(MainActivity.this,"您选择了【取消】",Toast.LENGTH_SHORT).show();
                        }
                    });
                    builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Buys.remove(position);
                            MyBuy.remove(position);
                            simpleAdapter.notifyDataSetChanged();
                            Toast.makeText(MainActivity.this,"您移除了第"+position+"个商品",Toast.LENGTH_SHORT).show();
                        }
                    });
                    builder.create().show();
                }
                return true;
            }
        });
    }

    //处理动态广播消息返回购物车界面
    @Override
    protected void onNewIntent(Intent intent){
        recyclerView.setVisibility(View.INVISIBLE);
        mlistview.setVisibility(View.VISIBLE);
        fab.setImageResource(R.mipmap.mainpage);
        flag = true;
    }
    //EventBus事件处理
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(Info a){
        Buys.add(a);//添加到购物车列表中
        Map<String,Object> temp = new LinkedHashMap<>();
        temp.put("cycle",String.valueOf(a.getcycle()));
        temp.put("name",a.getName());
        temp.put("price",a.getPrice());
        MyBuy.add(temp);//显示到购物车列表界面中
        simpleAdapter.notifyDataSetChanged();//更新列表
    }
    @Override
    public void onDestroy(){
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
    //添加新的商品
    public void add_newinfos(){
        LayoutInflater factory = LayoutInflater.from(this);
        final View textEntryView=factory.inflate(R.layout.add_info,null);
        final EditText add_name = (EditText) textEntryView.findViewById(R.id.add_name);
        final EditText add_price = (EditText) textEntryView.findViewById(R.id.add_price);
        final EditText add_type = (EditText) textEntryView.findViewById(R.id.add_type);
        final EditText add_infos = (EditText) textEntryView.findViewById(R.id.add_infos);

        AlertDialog.Builder add = new AlertDialog.Builder(MainActivity.this);
        add.setTitle("新增商品信息");
        add.setIcon(android.R.drawable.ic_dialog_info);
        add.setView(textEntryView);
        add.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Info add_info = new Info(add_name.getText().toString(),"¥"+add_price.getText().toString(),
                        add_type.getText().toString(),add_infos.getText().toString(),R.mipmap.sysu);
                Infos.add(add_info);
                mCommonAdapter.notifyItemInserted(Infos.size()-1);
                mCommonAdapter.notifyItemRangeChanged(Infos.size()-1,1);
                Toast.makeText(MainActivity.this,"添加商品成功",Toast.LENGTH_SHORT).show();
            }
        });
        add.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this,"您选择了取消",Toast.LENGTH_SHORT).show();
            }
        });
        add.show();
    }
    //处理点击添加到购物车时将商品加到购物车
//    @Override
//    protected void onActivityResult(int requestCode,int resultCode,Intent data){
//        if(resultCode == 1){
//            Bundle bundle=data.getExtras();
//            Info info=(Info) bundle.get("Info");
//            Buys.add(info);
//            Map<String,Object> temp = new LinkedHashMap<>();
//            temp.put("cycle",String.valueOf(info.getcycle()));
//            temp.put("name",info.getName());
//            temp.put("price",info.getPrice());
//            MyBuy.add(temp);
//            simpleAdapter.notifyDataSetChanged();
//        }
//    }
}
