package com.burytomorrow.my_lab3;

import java.io.Serializable;

/**
 * Created by BuryTomorrow on 2017/10/22.
 */

public class Info implements Serializable{
    //初始化
    public Info(String name,String price,String type,String goods_info,int imgSrc){
        this.name=name;
        this.price=price;
        this.type=type;
        this.goods_info=goods_info;
        this.imgSrc=imgSrc;
    }
    //获取名字和设置名字
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    //获取价格
    public String getPrice(){
        return price;
    }
    public void setPrice(String price){
        this.price=price;
    }
    //获取商品类型
    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type=type;
    }
    //获取商品类型信息
    public String getGoods_info(){
        return goods_info;
    }
    public void setGoods_info(String goods_info){
        this.goods_info=goods_info;
    }
    //获取商品对应的图片
    public int getImage(){
        return imgSrc;
    }
    public void setImage(int imgSrc){
        this.imgSrc=imgSrc;
    }

    //获取商品名字的首字母
    public char getcycle(){
        char first=name.charAt(0);
        if(first >=97&&first<=122){
            first -=32;
        }
        if (name.equals("假的购物车")){
            first = '*';
        }
        return first;
    }

    private String name;//商品名字
    private String price;//商品价格
    private String type;//商品类型
    private String goods_info;//商品类型的详细信息
    private int imgSrc;//商品对应的图像
}
