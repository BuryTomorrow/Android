package com.burytomorrow.mylab8;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public Button add;
    public ListView listView;
    public SimpleAdapter adapter;
    public String TABLE_NAME = "MyTable";
    public List<Map<String,String>> datas;
    private PopupWindow mPopupWindow;//下拉窗口
    private View topview;
    private int Width;//手机宽度
    private int Height;//手机高度
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WindowManager wm = this.getWindowManager();
        Width = wm.getDefaultDisplay().getWidth();
        Height = wm.getDefaultDisplay().getHeight();

        init();
        updateFromDB();
        listener();
    }
    public void init(){
        listView = (ListView) findViewById(R.id.listView);
        add = (Button) findViewById(R.id.add);
        topview = (View) findViewById(R.id.topview);
    }

    //更新列表
    public void updateFromDB(){
        mySQLite db = new mySQLite(getBaseContext());
        SQLiteDatabase sqLiteDatabase = db.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("select * from "+TABLE_NAME,null);
        datas = new ArrayList<Map<String, String>>();
        if(cursor!=null){
            while (cursor.moveToNext()){
                String name2 = cursor.getString(1);
                String birth2 = cursor.getString(2);
                String gift2 = cursor.getString(3);
                Map<String,String> m = new HashMap<String, String>();
                m.put("name",name2);
                m.put("birth",birth2);
                m.put("gift",gift2);
                datas.add(m);
            }
            adapter = new SimpleAdapter(MainActivity.this,datas,R.layout.items,
                    new String[]{"name","birth","gift"},
                    new int[]{R.id.name,R.id.birth,R.id.gift});
            listView.setAdapter(adapter);
        }
    }

    public void listener(){
        //listView点击事件
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        /* ***********************************↓↓这一部分是下拉框↓↓*******************************************/
//                //下拉窗实现
//                View popupView = MainActivity.this.getLayoutInflater().inflate(R.layout.popupview, null);
//                //为PopupWindow指定宽度和高度
//                mPopupWindow = new PopupWindow(popupView, Width, Height-2*Height /5);
//                //设置动画
//                mPopupWindow.setAnimationStyle(R.style.popup_window_anim);
//                //设置背景颜色
//                mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#757575")));
//                //设置可以获取焦点
//                mPopupWindow.setFocusable(true);
//                //设置可以触摸弹出框以外的区域
//                mPopupWindow.setOutsideTouchable(true);
//                //触摸popupwindow以外区域时使其消失
//                //popupwindow状态更新
//                mPopupWindow.update();
//                //以下拉的方式显示，并且可以设置显示的位置
//                mPopupWindow.showAsDropDown(topview);
//
//                final TextView name = (TextView) popupView.findViewById(R.id.pop_name);
//                final EditText birth = (EditText) popupView.findViewById(R.id.pop_birth);
//                final EditText gift = (EditText) popupView.findViewById(R.id.pop_gift);
//                final TextView phone = (TextView) popupView.findViewById(R.id.pop_phone);
//                phone.setMovementMethod(ScrollingMovementMethod.getInstance());
//                Button check = (Button) popupView.findViewById(R.id.checked);
//                Button cancel = (Button) popupView.findViewById(R.id.cancel);
        /* ***********************************↑↑这一部分是下拉框↑↑*******************************************/


        /* ***********************************↓↓这一部分是弹出框↓↓*******************************************/
                //对话框
                LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
                View mView = inflater.inflate(R.layout.dialog,null);
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

                final TextView name = (TextView) mView.findViewById(R.id.dialog_name);
                final EditText birth = (EditText) mView.findViewById(R.id.dialog_birth);
                final EditText gift = (EditText) mView.findViewById(R.id.dialog_gift);
                final TextView phone = (TextView) mView.findViewById(R.id.dialog_phone);
        /* ***********************************↑↑这一部分是弹出框↑↑*******************************************/

                name.setText(datas.get(position).get("name"));
                birth.setText(datas.get(position).get("birth"));
                gift.setText(datas.get(position).get("gift"));

                //需要先进行动态权限申请
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS},0);
                }

                //使用getContentResolver方法读取联系人列表
                String s="";
                Cursor cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                        null,null,null,null);
                while (cursor.moveToNext()){
                    String s2 = cursor.getString(cursor.getColumnIndex("_id"));
                    //判断是否有该联系人，并读取其电话
                    if(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)).equals(name.getText().toString())){
                        //判断是否有电话
                        if(Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)))>0){
                            Cursor phonenumber = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "="+ s2,null,null);
                            while (phonenumber.moveToNext()){
                                s+=phonenumber.getString(phonenumber.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))+"\n";
                            }
                            phonenumber.close();
                        }
                    }
                }
                cursor.close();
                if(s.equals("")) s="无联系人电话";
                phone.setText(s);

        /* ***********************************↓↓这一部分是弹出框↓↓*******************************************/
                //对话框实现
                builder.setView(mView);
                builder.setTitle("我只是个对话框（*-.-*）");
                builder.setPositiveButton("保存", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mySQLite db = new mySQLite(getBaseContext());
                        SQLiteDatabase sqLiteDatabase = db.getWritableDatabase();
                        if(birth.length()!=0){
                            sqLiteDatabase.execSQL("update "+ TABLE_NAME + " set birth = '"+ birth.getText().toString()
                                                    + "' where name = '"+ name.getText().toString()+"'");
                        }
                        if(gift.length()!=0){
                            sqLiteDatabase.execSQL("update "+ TABLE_NAME + " set gift = '"+ gift.getText().toString()
                                    + "' where name = '"+ name.getText().toString()+"'");
                        }
                        sqLiteDatabase.close();
                        updateFromDB();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
        /* ***********************************↑↑这一部分是弹出框↑↑*******************************************/

        /* ***********************************↓↓这一部分是下拉框↓↓*******************************************/
//                //下拉窗口的监听事件：
//                check.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mySQLite db = new mySQLite(getBaseContext());
//                        SQLiteDatabase sqLiteDatabase = db.getWritableDatabase();
//                        if(birth.length()!=0){
//                            sqLiteDatabase.execSQL("update "+ TABLE_NAME + " set birth = '"+ birth.getText().toString()
//                                                    + "' where name = '"+ name.getText().toString()+"'");
//                        }
//                        if(gift.length()!=0){
//                            sqLiteDatabase.execSQL("update "+ TABLE_NAME + " set gift = '"+ gift.getText().toString()
//                                    + "' where name = '"+ name.getText().toString()+"'");
//                        }
//                        sqLiteDatabase.close();
//                        updateFromDB();
//                        mPopupWindow.dismiss();
//                    }
//                });
//                cancel.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mPopupWindow.dismiss();
//                    }
//                });
        /* ***********************************↑↑这一部分是下拉框↑↑*******************************************/
            }
        });

        //listView长按事件
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setTitle("是否删除该联系人");
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //数据库中删除
                        mySQLite db = new mySQLite(getBaseContext());
                        SQLiteDatabase sqLiteDatabase = db.getWritableDatabase();
                        sqLiteDatabase.execSQL("delete from "+ TABLE_NAME + " where name = '"+datas.get(position).get("name")+"'");
                        sqLiteDatabase.close();
                        //删除listView相应数据
                        datas.remove(position);
                        adapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();
                return true;
            }
        });
        //增加按钮
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,AddActivity.class);
                startActivityForResult(intent,1);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode,int resultCode , Intent data ){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode == 1 && resultCode == 2){
            updateFromDB();
        }
    }
}
