package com.burytomorrow.mylab9;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Observable;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.OvershootInLeftAnimator;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private static GithubService service ;
    private RecyclerView recyclerView;
    private List<Map<String,Object>>  mGithubs;
    private Button clear;
    private Button fetch;
    private EditText editText;
    private ProgressBar progressBar;
    private CommonAdapter commonAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //需要先进行动态权限申请
        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.INTERNET)!= PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.INTERNET},0);
        }


        init();

        updateRecycle();

        listener();
    }

    public void init(){
        recyclerView = (RecyclerView) findViewById(R.id.recycle);
        clear = (Button) findViewById(R.id.clear);
        fetch = (Button) findViewById(R.id.fetch);
        editText = (EditText) findViewById(R.id.editText);
        progressBar = (ProgressBar) findViewById(R.id.bar);
        mGithubs = new ArrayList<>();
    }

    public void updateRecycle(){
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        commonAdapter = new CommonAdapter<Map<String,Object>>(this,R.layout.items,mGithubs){
            @Override
            public void convert(MyViewHolder holder, Map<String,Object> s){
                TextView id= holder.getView(R.id.id);
                TextView login = holder.getView(R.id.login);
                TextView blog = holder.getView(R.id.blog);
                login.setText(s.get("login").toString());
                id.setText("id:"+s.get("id").toString());
                blog.setText("blog:"+s.get("blog").toString());
            }
        };
        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(commonAdapter);
        animationAdapter.setDuration(200);
        recyclerView.setAdapter(animationAdapter);
        recyclerView.setItemAnimator(new OvershootInLeftAnimator());
    }

    public void listener(){
        //清空列表
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int length = mGithubs.size();
                mGithubs.clear();
                commonAdapter.notifyItemRangeRemoved(0,length);
            }
        });
        //添加新的User Model
        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                final String user = editText.getText().toString();
                service = createRetrofit("https://api.github.com/").create(GithubService.class);
                service.getUser(user)
                        .subscribeOn(Schedulers.newThread()) //新建线程进行网络访问
                        .observeOn(AndroidSchedulers.mainThread()) //主线程处理请求结果
                        .subscribe(new Subscriber<Github>(){
                            @Override
                            public final void onCompleted(){ //请求结束时调用的回调函数
                                progressBar.setVisibility(View.INVISIBLE);
                            }
                            @Override
                            public void onError(Throwable e){//出现错误使调用的函数
                                progressBar.setVisibility(View.INVISIBLE);
                                Toast.makeText(MainActivity.this,e.hashCode()+"请确认搜索的用户存在",Toast.LENGTH_SHORT).show();
                            }
                            @Override
                            public void onNext(Github github){ //收到每一次数据时调用的函数
                                Map<String,Object> temp = new LinkedHashMap<>();
                                temp.put("id",github.getId());
                                temp.put("blog",github.getBlog());
                                temp.put("login",github.getLogin());
                                temp.put("user",user);
                                mGithubs.add(temp);
                                commonAdapter.notifyItemInserted(mGithubs.size()-1);
                                commonAdapter.notifyItemRangeChanged(mGithubs.size()-1,1);
                            }
                        });
            }
        });

        //点击事件
        commonAdapter.setOnItemClickListener(new CommonAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {
                Intent intent = new Intent(MainActivity.this,ReposActivity.class);
                String user = mGithubs.get(position).get("user").toString();
                intent.putExtra("user",user);
                startActivity(intent);
            }

            @Override
            public void onLongClick(int position) {
                mGithubs.remove(position);
                commonAdapter.notifyItemRemoved(position);
            }
        });
    }

    private static OkHttpClient createOkHttp(){
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                                        .connectTimeout(10, TimeUnit.SECONDS)//连接超时
                                        .readTimeout(30,TimeUnit.SECONDS)//读超时
                                        .writeTimeout(10,TimeUnit.SECONDS)//写超时
                                        .build();
        return okHttpClient;
    }
    private static Retrofit createRetrofit(String baseUrl){
        return new Retrofit.Builder()
                           .baseUrl(baseUrl) //设置baseUrl
                           .addConverterFactory(GsonConverterFactory.create())//添加GSON Converter
                           .addCallAdapterFactory(RxJavaCallAdapterFactory.create())//RxJavaCall Adapter
                           .client(createOkHttp())//okHttp
                           .build();
    }
}
