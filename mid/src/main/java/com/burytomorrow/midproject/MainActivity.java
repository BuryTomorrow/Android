package com.burytomorrow.midproject;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import jp.wasabeef.recyclerview.adapters.ScaleInAnimationAdapter;
import jp.wasabeef.recyclerview.animators.OvershootInLeftAnimator;

public class MainActivity extends AppCompatActivity {
    /*在这里声明变量*/
    private List<Person> persons;
    private RecyclerView mRecycle;//列表
    private CommonAdapter<Person> mCommonAdapter;//自定义列表适配器
    private PopupWindow mPopupWindow;//下拉窗口
    private RelativeLayout topview;//顶部
    private LinearLayout midView;//中部搜索框
    private LinearLayout hideMenu;//删除模式下才显示的菜单
    private int Width;//手机宽度
    private int Height;//手机高度
    //筛选部分定义：
    private String select_nation = "all";//选择所属势力,未点击确认前
    private String select_sex = "all";//选择性别，未点击确认前
    private String selected_nation = "all";//选择所属势力
    private String selected_sex = "all";//选择性别
    private String selected_name = "";//选择名字，搜索框

    //下拉窗口按钮定义：
    private RadioGroup for_nation;//关于所属势力的按钮组
    private RadioButton all_1;//全部
    private RadioButton wei;//魏
    private RadioButton shu;//蜀
    private RadioButton wu;//吴
    private RadioGroup for_sex;//关于性别的按钮组
    private RadioButton all_2;//全部
    private RadioButton man;//男
    private RadioButton lady;//女
    private Button checked;//确认按钮

    //搜索输入框和按钮定义
    private EditText searchText;//搜索输入框
    private Button search;//搜索按钮
    private ImageButton search_reset;//重置输入框按钮

    //悬浮按钮定义:
    private boolean menuOpen = false; //悬浮按钮显示判断
    private FloatingActionButton setting;//设置
    private FloatingActionButton add;//增加按钮
    private FloatingActionButton stop;//静音按钮
    private FloatingActionButton select;//筛选按钮
    private boolean forDelete = false; //删除模式判断
    private boolean musicStop = false;//是否停止播放音乐

    //删除模式下隐藏的菜单的按钮定义：
    private Button deleteAll;//全选按钮
    private ImageButton checkDelete;//确认删除
    private Button cancel;//取消删除，退出删除模式
    private boolean isDeleteAll = false;//用于判断是否已经全选
    AlertDialog.Builder builder;//弹出框
    // 数据库相关声明
    DB_Helper db_helper;

    //音乐播放定义
    private MediaPlayer mPlayer;

    /*-------------*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WindowManager wm = this.getWindowManager();
        Width = wm.getDefaultDisplay().getWidth();
        Height = wm.getDefaultDisplay().getHeight();

        verifyPermissions(this); // 申请读取权限

        dialog();//弹出框设置

        buttonSet();//悬浮按钮以及相关按钮设置

        popupWindow();//设置下拉窗口

        init_data();//初始化数据，显示第一个界面

        interface_1();//第一个界面

        interface_1_listener();//自定义列表的点击事件

        search_listener();//搜索输入框事件
    }

    void dialog() {
        builder = new AlertDialog.Builder(this);
        builder.setTitle("删除后无法恢复，是否确认删除？")
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "请重新进行操作", Toast.LENGTH_SHORT).show();
                    }
                })
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        for (int i = 0; i < persons.size(); ) {
                            if (persons.get(i).forDelete) { //删除项
                                mDelete(persons.get(i).name);
                                persons.remove(i);
                                mCommonAdapter.notifyItemRemoved(i);
                            } else i++;
                        }
                        outDeleteMode();
                    }
                });
    }

    //这里面有四个按钮，景韬要搞定增加按钮，增加可能需要写多一个界面，或者其他方法，讨论下
    //然后在这里面设置监听事件，事件的具体内容用函数包起来或者直接写这里面也行
    /*------------------------------------版本3begin--------------------------------------------*/
    void buttonSet() {//悬浮按钮设置
        setting = (FloatingActionButton) findViewById(R.id.setting);//设置按钮
        add = (FloatingActionButton) findViewById(R.id.add);//增加按钮
        stop = (FloatingActionButton) findViewById(R.id.stop);//删除按钮
        select = (FloatingActionButton) findViewById(R.id.select);//筛选按钮
        midView = (LinearLayout) findViewById(R.id.mid);//中部的搜索框，进入删除模式时隐藏

        hideMenu = (LinearLayout) findViewById(R.id.hidemenu);//删除模式下出现的菜单
        deleteAll = (Button) findViewById(R.id.delete_all);//全选按钮
        checkDelete = (ImageButton) findViewById(R.id.checkdelect);//确认删除
        cancel = (Button) findViewById(R.id.cancel);//取消删除，退出删除模式

        mPlayer = new MediaPlayer();
        mPlayer = MediaPlayer.create(this, R.raw.music5);
        mPlayer.start();
        mPlayer.setLooping(true);

        //悬浮设置按钮的点击事件
        setting.setOnClickListener(new FloatingActionButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!forDelete) { //只能在非删除模式下使用
                    if (menuOpen) { //如果悬浮按钮已经打开
                        hideMenu();
                    } else {// 如果悬浮按钮还没打开
                        showMenu();
                    }
                }
            }
        });

        //音乐按钮的点击事件
        stop.setOnClickListener(new FloatingActionButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (musicStop) { //如果原本是停止播放音乐，那就开启音乐
                    mPlayer.start();
                    musicStop = false;
                    stop.setImageResource(R.mipmap.music_on);
                } else {//否则就停止播放音乐
                    mPlayer.pause();
                    musicStop = true;
                    stop.setImageResource(R.mipmap.music_off);
                }
            }
        });
        /*------------------------模式相关按钮设置--------------------------*/
        //全选按钮设置
        deleteAll.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDeleteAll) { //已经全选了
                    isDeleteAll = false;
                    deleteAll.setBackgroundResource(R.drawable.deletebutton_unchecked);
                    deleteAll.setText("全选");
                    for (int i = 0; i < persons.size(); i++) {
                        persons.get(i).forDelete = false;
                    }
                } else {// 没有全选
                    isDeleteAll = true;
                    deleteAll.setBackgroundResource(R.drawable.deletebutton_checked);
                    deleteAll.setText("全不选");
                    for (int i = 0; i < persons.size(); i++) {
                        persons.get(i).forDelete = true;
                    }
                }
                mCommonAdapter.notifyItemRangeChanged(0, persons.size());//更新列表
            }
        });
        //确认删除按钮设置
        checkDelete.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.create().show();
            }
        });
        //取消按钮设置
        cancel.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                outDeleteMode();
            }
        });

        //添加人物按钮的点击事件
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideMenu();
                Intent add_intent = new Intent(MainActivity.this, Person_add.class);
                startActivityForResult(add_intent, 2);
            }
        });

    }

    void inDeleteMode() {
        forDelete = true;//进入删除模式
        mCommonAdapter.notifyItemRangeChanged(0, persons.size());//更新列表
        midView.setVisibility(View.GONE);
        hideMenu.setVisibility(View.VISIBLE);
        hideMenu();
        hideButton();
    }

    void outDeleteMode() { //退出删除模式函数
        forDelete = false;//退出删除模式
        mCommonAdapter.notifyItemRangeChanged(0, persons.size());//更新列表
        midView.setVisibility(View.VISIBLE);
        isDeleteAll = false;//初始化全选按钮
        deleteAll.setBackgroundResource(R.drawable.deletebutton_unchecked);
        hideMenu.setVisibility(View.GONE);
        for (int i = 0; i < persons.size(); i++) {
            persons.get(i).forDelete = false;
        }
        showButton();
    }
    /*------------------------------------版本3end----------------------------------------------*/

    //下拉窗口
    void popupWindow() {
        topview = (RelativeLayout) findViewById(R.id.Top);
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideMenu();
                View popupView = MainActivity.this.getLayoutInflater().inflate(R.layout.popupview, null);
                //为PopupWindow指定宽度和高度
                mPopupWindow = new PopupWindow(popupView, Width, Height / 2);
                //设置动画
                mPopupWindow.setAnimationStyle(R.style.popup_window_anim);
                //设置背景颜色
                mPopupWindow.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#00FFFFFF")));
                //设置可以获取焦点
                mPopupWindow.setFocusable(true);
                //设置可以触摸弹出框以外的区域
                mPopupWindow.setOutsideTouchable(true);
                //触摸popupwindow以外区域时使其消失
                //popupwindow状态更新
                mPopupWindow.update();
                //以下拉的方式显示，并且可以设置显示的位置
                mPopupWindow.showAsDropDown(topview);

                //下拉窗口的findViewById:
                for_nation = (RadioGroup) popupView.findViewById(R.id.group1);
                all_1 = (RadioButton) popupView.findViewById(R.id.all_button1);
                wei = (RadioButton) popupView.findViewById(R.id.wei_button);
                shu = (RadioButton) popupView.findViewById(R.id.shu_button);
                wu = (RadioButton) popupView.findViewById(R.id.wu_button);
                for_sex = (RadioGroup) popupView.findViewById(R.id.group2);
                all_2 = (RadioButton) popupView.findViewById(R.id.all_button2);
                man = (RadioButton) popupView.findViewById(R.id.man_button);
                lady = (RadioButton) popupView.findViewById(R.id.lady_button);
                checked = (Button) popupView.findViewById(R.id.checkselect);

                //根据原本选择的属性值来
                if (selected_nation.equals("all")) {
                    all_1.setChecked(true);
                } else if (selected_nation.equals("魏")) {
                    wei.setChecked(true);
                } else if (selected_nation.equals("蜀")) {
                    shu.setChecked(true);
                } else if (selected_nation.equals("吴")) {
                    wu.setChecked(true);
                }

                if (selected_sex.equals("all")) {
                    all_2.setChecked(true);
                } else if (selected_sex.equals("男")) {
                    man.setChecked(true);
                } else if (selected_sex.equals("女")) {
                    lady.setChecked(true);
                }
                group_listener();//下拉窗口的点击事件
            }
        });
    }

    //初始化数据，显示界面
    void init_data() {
        //读取数据库
        db_helper = new DB_Helper(this);
        SQLiteDatabase db = db_helper.getReadableDatabase();
        //选择所有人物属性：
        Cursor cursor = db.rawQuery("SELECT * FROM Person", null);

        persons = new ArrayList<>();//初始化数组
        if (cursor.moveToFirst()) {
            do {
                //添加人物信息
                persons.add(
                        new Person(
                                cursor.getString(cursor.getColumnIndex("nation")),
                                cursor.getString(cursor.getColumnIndex("name")),
                                cursor.getString(cursor.getColumnIndex("sex")),
                                cursor.getString(cursor.getColumnIndex("place")),
                                cursor.getString(cursor.getColumnIndex("birth")),
                                cursor.getString(cursor.getColumnIndex("info")),
                                cursor.getString(cursor.getColumnIndex("picture")),
                                cursor.getString(cursor.getColumnIndex("picture_info"))
                        )
                );
            } while (cursor.moveToNext());
        }
    }

    //设置第一个界面
    void interface_1() {
        mRecycle = (RecyclerView) findViewById(R.id.recycler);
        mRecycle.setLayoutManager(new GridLayoutManager(this, 3));
        mRecycle.addItemDecoration(new SpaceItemDecoration(0 - Width / 18));
        mCommonAdapter = new CommonAdapter<Person>(this, R.layout.personlist, persons) {
            @Override
            public void convert(MyViewHolder holder, Person s) {
                ImageView img = holder.getView(R.id.personImg);
                img.setImageURI(Uri.parse(s.picture));
                TextView name = holder.getView(R.id.personName);
                name.setText(s.name);
                if (forDelete) { //如果是删除模式
                    ImageView del = holder.getView(R.id.fordelete);
                    del.setVisibility(View.VISIBLE);
                    if (s.forDelete) { //删除模式下该元素被选择到的话
                        del.setImageResource(R.mipmap.right);
                        del.setPadding(40, 40, 40, 40);
                    } else {
                        del.setImageResource(R.drawable.cardborder);
                        del.setPadding(0, 0, 0, 0);
                    }
                } else {//如果不是删除模式的话
                    ImageView del = holder.getView(R.id.fordelete);
                    del.setVisibility(View.INVISIBLE);
                }
            }
        };
        ScaleInAnimationAdapter animationAdapter = new ScaleInAnimationAdapter(mCommonAdapter);
        animationAdapter.setDuration(600);
        mRecycle.setAdapter(animationAdapter);
        mRecycle.setItemAnimator(new OvershootInLeftAnimator());
    }

    //自定义列表的点击事件
    /*------------------------------------版本2begin--------------------------------------------*/
    //浪浪负责：
    void interface_1_listener() {
        mCommonAdapter.setOnItemClickListener(new CommonAdapter.OnItemClickListener() {
            @Override
            public void onClick(int position) {//点击事件
                if (forDelete) { //删除模式
                    if (persons.get(position).forDelete) {
                        persons.get(position).forDelete = false;
                    } else {
                        persons.get(position).forDelete = true;
                    }
                    mCommonAdapter.notifyItemRangeChanged(position, 1);//更新item
                } else {//正常模式
                    Intent pI_intent = new Intent(MainActivity.this, Person_Information.class);
                    Person person_Information = persons.get(position);
                    pI_intent.putExtra("person", person_Information);
                    startActivityForResult(pI_intent, 1);
                }
            }

            //长按事件,暂时不做
            @Override
            public void onLongClick(int position) {
                if (!forDelete) {
                    inDeleteMode();
                }
            }
        });
    }
    /*------------------------------------版本2end----------------------------------------------*/

    //搜索输入框事件
    void search_listener() {
        searchText = (EditText) findViewById(R.id.searchname);
        search = (Button) findViewById(R.id.search);
        search_reset = (ImageButton) findViewById(R.id.search_reset);

        //点击搜索按钮，开始确认输入框内容然后筛选
        search.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                selected_name = searchText.getText().toString();
                searchText.setText("");
                search.setFocusable(true);
                search.setFocusableInTouchMode(true);
                search.requestFocus();
                UpdateList();
            }
        });
        //点击重置按钮，重置输入框内容
        search_reset.setOnClickListener(new ImageButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setText("");
            }
        });
    }

    //下拉窗口的点击事件,由popupWindow()调用
    void group_listener() {
        //所属势力选择的点击事件
        for_nation.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (all_1.isChecked()) {//全选
                    select_nation = "all";
                } else if (wei.isChecked()) {//魏
                    select_nation = "魏";
                } else if (shu.isChecked()) {//蜀
                    select_nation = "蜀";
                } else if (wu.isChecked()) {//吴
                    select_nation = "吴";
                }
            }
        });
        for_sex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (all_2.isChecked()) { //全选
                    select_sex = "all";
                } else if (man.isChecked()) {//选择男
                    select_sex = "男";
                } else if (lady.isChecked()) {//选择女
                    select_sex = "女";
                }
            }
        });
        //当确定按钮点击的时候应该更新显示的列表，这里需要数据库操作，景韬搞定
        checked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //以下为输出的参考码：
                selected_nation = select_nation;
                selected_sex = select_sex;
                UpdateList();//更新显示列表的数据，显示列表为persons，应该更新它
                //这一句是让下拉窗口消失
                mPopupWindow.dismiss();
            }
        });
    }

    /*------------------------------------版本2begin--------------------------------------------*/
    //景韬负责这个部分，由下拉菜单的确认按钮点击时触发，后面添加在搜索按钮点击时也调用该函数
    void UpdateList() {
        int list_length = persons.size();
        persons.clear(); // 清空原先的list
        mCommonAdapter.notifyItemRangeRemoved(0, list_length);
        String attribute[] = {"nation", "sex"}; // 两种属性
        String attrValue[][] = new String[2][]; // 定义两行，每行不知道几列的二维数组

        if (selected_nation.equals("all")) { // 如果是all，则nation属性有三个取值
            attrValue[0] = new String[3];
            attrValue[0][0] = "蜀";
            attrValue[0][1] = "魏";
            attrValue[0][2] = "吴";
        } else {
            attrValue[0] = new String[1];
            attrValue[0][0] = selected_nation;
        } // 否则取selected_nation

        if (selected_sex.equals("all")) { // 如果是all，则sex属性有两个取值
            attrValue[1] = new String[2];
            attrValue[1][0] = "男";
            attrValue[1][1] = "女";
        } else {
            attrValue[1] = new String[1];
            attrValue[1][0] = selected_sex;
        } // 否则取selected_sex

//        attrValue[2]=new String[1];attrValue[2][0]=selected_name;

        Cursor cursor = mQuery(attribute, attrValue);
        if (cursor.moveToFirst()) {
            do {
                persons.add(
                        new Person(
                                cursor.getString(cursor.getColumnIndex("nation")),
                                cursor.getString(cursor.getColumnIndex("name")),
                                cursor.getString(cursor.getColumnIndex("sex")),
                                cursor.getString(cursor.getColumnIndex("place")),
                                cursor.getString(cursor.getColumnIndex("birth")),
                                cursor.getString(cursor.getColumnIndex("info")),
                                cursor.getString(cursor.getColumnIndex("picture")),
                                cursor.getString(cursor.getColumnIndex("picture_info"))
                        )
                );
            } while (cursor.moveToNext());
        }
        list_length = persons.size();
        mCommonAdapter.notifyItemRangeInserted(0, list_length);
        /*参考更新代码：
        notifyItemRangeInserted(int positionStart, int itemCount)
        列表从positionStart位置到itemCount数量的列表项批量添加数据时调用，伴有动画效果
        notifyItemRangeRemoved(int positionStart, int itemCount)
        列表从positionStart位置到itemCount数量的列表项批量删除数据时调用，伴有动画效果
        */
    }

    // 增(insert)删(delete)改(update)查(query)函数
    void mInsert(Person person) {
        ContentValues values = new ContentValues();
        values.put("nation", person.nation);
        values.put("name", person.name);
        values.put("sex", person.sex);
        values.put("place", person.place);
        values.put("birth", person.birth);
        values.put("info", person.info);
        values.put("picture", person.picture);
        values.put("picture_info", person.picture_info);
        db_helper.getWritableDatabase().insert("Person", null, values);
    }

    void mDelete(String s) {
        db_helper.getWritableDatabase().execSQL("DELETE FROM PERSON WHERE name ='" + s + "' ");
    }

    void mUpdate(Person p) {
        String sql = "UPDATE PERSON set place='" + p.place + "', birth='" + p.birth + "', info='"
                      + p.info + "' WHERE name='" + p.name + "'";
        db_helper.getWritableDatabase().execSQL(sql);
        int position = 0;
        for (int i = 0; i < persons.size(); i++) {
            if (persons.get(i).name.equals(p.name)) {
                position = i;
                break;
            }
        }
        persons.get(position).place = p.place;
        persons.get(position).birth = p.birth;
        persons.get(position).info = p.info;
        mCommonAdapter.notifyItemChanged(position);
    }

    Cursor mQuery(String attribute[], String attrValue[][]) {
        // 传入属性名称与对应的属性值 进行筛选
        String sqlStatement = "SELECT * FROM PERSON ";
        int length = attribute.length;
        if (length != 0) {
            sqlStatement += "WHERE ";
            for (int i = 0; i < length; i++) {
                sqlStatement += "(";
                for (int j = 0; j < attrValue[i].length; j++) {
                    sqlStatement += attribute[i];
                    sqlStatement += " = '";
                    sqlStatement += attrValue[i][j];
                    sqlStatement += "' ";
                    if (j != attrValue[i].length - 1) sqlStatement += "OR ";
                }
                sqlStatement += ")";
                if (i != length - 1) sqlStatement += "AND ";
            }
            if (selected_name.equals("") == false) {
                sqlStatement += " AND name =" + "'" + selected_name + "' ";
            }
        }
        System.out.println(sqlStatement);

        return db_helper.getReadableDatabase().rawQuery(sqlStatement, null);
    }
    /*------------------------------------版本2end----------------------------------------------*/

    //显示悬浮按钮
    void showMenu() {
        menuOpen = true;
        setting.setImageResource(R.mipmap.reset);
        int x = (int) setting.getX();//获取设置按钮的位置
        int y = (int) setting.getY();
        //新增模式的按钮往左边偏移
        ValueAnimator v1 = ValueAnimator.ofInt(x, x - 3 * setting.getWidth() / 2);//偏移位置
        v1.setDuration(300);
        v1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int l = (int) animator.getAnimatedValue();
                int t = (int) add.getY();
                int r = add.getWidth() + l;
                int b = add.getHeight() + t;
                add.layout(l, t, r, b);
            }
        });
        //删除按钮往左上偏移
        ValueAnimator v2x = ValueAnimator.ofInt(x, x - setting.getWidth());
        ValueAnimator v2y = ValueAnimator.ofInt(y, y - setting.getWidth());
        v2x.setDuration(300);
        v2x.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int l = (int) animator.getAnimatedValue();
                int t = (int) stop.getY();
                int r = stop.getWidth() + l;
                int b = stop.getHeight() + t;
                stop.layout(l, t, r, b);
            }
        });
        v2y.setDuration(300).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int t = (int) animation.getAnimatedValue();
                int l = (int) stop.getX();
                int r = stop.getWidth() + l;
                int b = stop.getHeight() + t;
                stop.layout(l, t, r, b);
            }
        });

        ValueAnimator v3 = ValueAnimator.ofInt(y, y - 3 * setting.getWidth() / 2);
        v3.setDuration(300);
        v3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                int t = (int) animator.getAnimatedValue();
                int l = (int) select.getX();
                int r = select.getWidth() + l;
                int b = select.getHeight() + t;
                select.layout(l, t, r, b);
            }
        });
        v1.start();
        v2x.start();
        v2y.start();
        v3.start();
    }
    //隐藏悬浮按钮
    void hideMenu() {
        menuOpen = false;
        setting.setImageResource(R.mipmap.setting);
        int x = (int) add.getX();
        ValueAnimator v1 = ValueAnimator.ofInt(x, (int) setting.getX());
        v1.setDuration(300).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int l = (int) animation.getAnimatedValue();
                int t = (int) add.getY();
                int r = add.getWidth() + l;
                int b = add.getHeight() + t;
                add.layout(l, t, r, b);
            }
        });
        x = (int) stop.getX();
        int y = (int) stop.getY();
        ValueAnimator v2x = ValueAnimator.ofInt(x, (int) setting.getX());
        ValueAnimator v2y = ValueAnimator.ofInt(y, (int) setting.getY());
        v2x.setDuration(300).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int l = (int) animation.getAnimatedValue();
                int t = (int) stop.getY();
                int r = stop.getWidth() + l;
                int b = stop.getHeight() + t;
                stop.layout(l, t, r, b);
            }
        });
        v2y.setDuration(300).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int t = (int) animation.getAnimatedValue();
                int l = (int) stop.getX();
                int r = stop.getWidth() + l;
                int b = stop.getHeight() + t;
                stop.layout(l, t, r, b);
            }
        });
        y = (int) select.getY();
        ValueAnimator v3 = ValueAnimator.ofInt(y, (int) setting.getY());
        v3.setDuration(300).addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int t = (int) animation.getAnimatedValue();
                int l = (int) select.getX();
                int r = select.getWidth() + l;
                int b = select.getHeight() + t;
                select.layout(l, t, r, b);
            }
        });
        v1.start();
        v2x.start();
        v2y.start();
        v3.start();
    }
    //显示所有悬浮按钮
    void showButton() {
        setting.setVisibility(View.VISIBLE);
        add.setVisibility(View.VISIBLE);
        stop.setVisibility(View.VISIBLE);
        select.setVisibility(View.VISIBLE);
    }

    //隐藏所有悬浮按钮
    void hideButton() {
        setting.setVisibility(View.GONE);
        add.setVisibility(View.GONE);
        stop.setVisibility(View.GONE);
        select.setVisibility(View.GONE);
    }

    // 返回别的activity传回的东西
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            if (requestCode == 2) {
                Person person = (Person) data.getSerializableExtra("person_add");
                Toast.makeText(MainActivity.this, person.sex + " " + person.nation + " " + person.name
                                + " " + person.place + " " + person.birth + " " + person.info + " " + person.picture,
                        Toast.LENGTH_LONG).show();
                mInsert(person);
                persons.add(person);
                mCommonAdapter.notifyDataSetChanged();
            }
        } else if (resultCode == 6) {
            Person person = (Person) data.getSerializableExtra("person");
            Toast.makeText(MainActivity.this, person.place, Toast.LENGTH_SHORT).show();
            mUpdate(person);
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    // 添加权限
    public static void verifyPermissions(Activity activity) {
        try {
            // 检测是否有读取权限
            int permission = ActivityCompat.checkSelfPermission(
                    activity, "android.permission.READ_EXTERNAL_STORAGE"
            );
            if (permission != PackageManager.PERMISSION_GRANTED) {
                // 没有读取的权限，就去申请读取的权限
                ActivityCompat.requestPermissions(activity,
                        new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
