package com.burytomorrow.midproject;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ScrollingTabContainerView;
import android.text.Layout;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;


/**
 * Created by Dylan on 2017/11/20.
 */

public class Person_Information extends AppCompatActivity {
    private String place;
    private String name;
    private String birth;
    private String info;
    private String nation;
    private String picture_info;
    private String picture;
    private int resultcode=5;//=6的时候代表着有更改
    @Override
    protected void onCreate(Bundle deliverBundle) {
        super.onCreate(deliverBundle);
        setContentView(R.layout.person_information);

        final Intent intent = this.getIntent();
        final Person person =(Person) intent.getSerializableExtra("person");
        picture_info = person.picture_info;
        nation = person.nation;
        place = person.place;
        name = person.name;
        birth = person.birth;
        info = person.info;
        picture = person.picture;

        final TextView Place = (TextView)findViewById(R.id.place);
        final TextView Birth = (TextView)findViewById(R.id.birth);
        final TextView Info = (TextView)findViewById(R.id.jianjie);
        final ImageView person_Background = (ImageView)findViewById(R.id.background);
        ImageView Nation = (ImageView)findViewById(R.id.nation);
        ImageButton Back = (ImageButton)findViewById(R.id.back);
        final View back_back_ground =findViewById(R.id.back_back_ground);
        final TextView Name = (TextView)findViewById(R.id.name);


        Place.setText(place);
        Birth.setText(birth);

        Info.setText(info);
        Info.setMovementMethod(ScrollingMovementMethod.getInstance());
        //person_Background.setImageResource(picture_info);
        person_Background.setImageURI(Uri.parse(picture_info));
        Place.setSingleLine(true);
        Place.setMovementMethod(ScrollingMovementMethod.getInstance());

        Typeface typeface = Typeface.createFromAsset(getAssets(),"xingshu.ttf");
        Name.setTypeface(typeface);
        Name.setText(name);

        if(nation.equals("蜀")){Nation.setImageResource(R.drawable.shu);
            back_back_ground.setBackgroundResource(R.drawable.bk_shu);}
        else if(nation.equals("魏")){Nation.setImageResource(R.drawable.wei);
            back_back_ground.setBackgroundResource(R.drawable.bk_wei);}
        else{Nation.setImageResource(R.drawable.wu);
            back_back_ground.setBackgroundResource(R.drawable.bk_wu);}

        //点击返回后回到原始界面--传回数据 resultcode==6则代表修改过
        Back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Person return_person = new Person();
                return_person.birth=birth;
                return_person.info=info;
                return_person.name=name;
                return_person.nation=nation;
                return_person.picture_info=picture_info;
                return_person.place=place;
                return_person.picture=picture;
                Intent intent2=new Intent();
                intent2.putExtra("person",return_person);
                setResult(resultcode,intent2);
                finish();
            }
        });


        //点击修改
        Place.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final EditText et = new EditText(Person_Information.this);//用于修改的文本框
                et.setText(place);
                new AlertDialog.Builder(Person_Information.this).setTitle("修改简历")
                        .setView(et)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String input =et.getText().toString();
                                if(input.equals("")){
                                    Toast.makeText(Person_Information.this,"籍贯不能为空!",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    place = input;
                                    resultcode=6;
                                    Place.setText(place);
                                }
                            }
                        })
                        .setNegativeButton("取消",null)
                        .show();
                return false;
            }
        });

        Birth.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final EditText et = new EditText(Person_Information.this);//用于修改的文本框
                et.setText(birth);
                new AlertDialog.Builder(Person_Information.this).setTitle("修改生卒年月")
                        .setView(et)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String input =et.getText().toString();
                                if(input.equals("")){
                                    Toast.makeText(Person_Information.this,"生卒年月不能为空!",Toast.LENGTH_SHORT).show();
                                }
                                else{
                                    birth = input;
                                    //Toast.makeText(Person_Information.this,birth,Toast.LENGTH_SHORT).show();
                                    Birth.setText(birth);
                                    resultcode=6;
                                }


                            }
                        })
                        .setNegativeButton("取消",null)
                        .show();
                return false;
            }
        });


        Info.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                final EditText et = new EditText(Person_Information.this);//用于修改的文本框
                et.setText(info);
                new AlertDialog.Builder(Person_Information.this).setTitle("修改简历")
                        .setView(et)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                String input =et.getText().toString();
                                if(input.equals("")){
                                    Toast.makeText(Person_Information.this,"简历不能为空!",Toast.LENGTH_SHORT).show();
                                }
                                else  {
                                    info = input;
                                    //Toast.makeText(Person_Information.this,info,Toast.LENGTH_SHORT).show();
                                    Info.setText(info);
                                    resultcode=6;
                                }


                            }
                        })
                        .setNegativeButton("取消",null)
                        .show();
                return false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        Person return_person = new Person();
        return_person.birth=birth;
        return_person.info=info;
        return_person.name=name;
        return_person.nation=nation;
        return_person.picture_info=picture_info;
        return_person.place=place;
        return_person.picture=picture;
        Intent intent2=new Intent();
        intent2.putExtra("person",return_person);
        setResult(resultcode,intent2);
        finish();
        super.onBackPressed();
    }
}
