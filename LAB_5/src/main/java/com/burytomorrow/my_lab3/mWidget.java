package com.burytomorrow.my_lab3;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.Toast;

/**
 * Implementation of App Widget functionality.
 */
public class mWidget extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        RemoteViews updateViews = new RemoteViews(context.getPackageName(),R.layout.m_widget);
        Intent i = new Intent(context,MainActivity.class);//点击事件进入主界面
        PendingIntent pi = PendingIntent.getActivity(context,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
        updateViews.setOnClickPendingIntent(R.id.RE,pi);//设定widget点击事件，绑定对应的Intent
        ComponentName na = new ComponentName(context,mWidget.class);
        appWidgetManager.updateAppWidget(na,updateViews);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
    @Override
    public void onReceive(Context context,Intent intent){

        super.onReceive(context,intent);
        if(intent.getAction().equals("com.example.ex4.MyStaticfliter")){

            Info info=(Info) intent.getSerializableExtra("Info");
            RemoteViews updateViews = new RemoteViews(context.getPackageName(),R.layout.m_widget);
            Intent i = new Intent(context,MoreInfomation.class);

            i.addCategory(Intent.CATEGORY_DEFAULT);

            i.putExtras(intent.getExtras());
            PendingIntent pi = PendingIntent.getActivity(context,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
            updateViews.setTextViewText(R.id.winfos,info.getName()+"仅售"+info.getPrice()+"!");
            updateViews.setImageViewResource(R.id.wImg,info.getImage());
            updateViews.setOnClickPendingIntent(R.id.RE,pi);
            ComponentName na = new ComponentName(context,mWidget.class);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.updateAppWidget(na,updateViews);
        }else if (intent.getAction().equals("com.example.ex4.MyDynamicFliter")){
            Info info=(Info) intent.getSerializableExtra("Info");
            RemoteViews updateViews = new RemoteViews(context.getPackageName(),R.layout.m_widget);
            Intent i = new Intent(context,MainActivity.class);

            i.addCategory(Intent.CATEGORY_DEFAULT);

            i.putExtras(intent.getExtras());
            PendingIntent pi = PendingIntent.getActivity(context,0,i,PendingIntent.FLAG_UPDATE_CURRENT);
            updateViews.setTextViewText(R.id.winfos,info.getName()+"已添加到购物车!");
            updateViews.setImageViewResource(R.id.wImg,info.getImage());
            updateViews.setOnClickPendingIntent(R.id.RE,pi);
            ComponentName na = new ComponentName(context,mWidget.class);
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            appWidgetManager.updateAppWidget(na,updateViews);
        }
    }
}

