package com.burytomorrow.midproject;

/**
 * Created by BuryTomorrow on 2017/11/19.
 */

import android.content.Context;

/**
 * Created by CJT on 2017/11/12.
 */

public class Data {

    public Person shu_persons[];
    public Person wei_persons[];
    public Person wu_persons[];


    Data(Context context){

        String picture_src_top = "android.resource://" + context.getPackageName() + "/" ;

        shu_persons = new Person[13] ;
        wei_persons = new Person[12] ;
        wu_persons = new Person[13] ;

        // Shu
        String shu_name[] = context.getResources().getStringArray(R.array.Shu_name) ;
        String shu_sex[] = context.getResources().getStringArray(R.array.Shu_sex) ;
        String shu_place[] = context.getResources().getStringArray(R.array.Shu_place) ;
        String shu_birth[] = context.getResources().getStringArray(R.array.Shu_birth) ;
        String shu_info[] = context.getResources().getStringArray(R.array.Shu_info) ;
        String shu_picture[] ={
                picture_src_top + R.mipmap.guanyu,
                picture_src_top + R.mipmap.huangzhong,
                picture_src_top + R.mipmap.jiangwei,
                picture_src_top + R.mipmap.liaohua,
                picture_src_top + R.mipmap.liubei,
                picture_src_top + R.mipmap.liushan,
                picture_src_top + R.mipmap.machao,
                picture_src_top + R.mipmap.masu,
                picture_src_top + R.mipmap.pangtong,
                picture_src_top + R.mipmap.weiyan,
                picture_src_top + R.mipmap.zhangfei,
                picture_src_top + R.mipmap.zhaoyun,
                picture_src_top + R.mipmap.zhugeliang
        };
        String shu_picture_info[] ={
                picture_src_top + R.mipmap.guanyu_info,
                picture_src_top + R.mipmap.huangzhong_info,
                picture_src_top + R.mipmap.jiangwei_info,
                picture_src_top + R.mipmap.liaohua_info,
                picture_src_top + R.mipmap.liubei_info,
                picture_src_top + R.mipmap.liushan_info,
                picture_src_top + R.mipmap.machao_info,
                picture_src_top + R.mipmap.masu_info,
                picture_src_top + R.mipmap.pangtong_info,
                picture_src_top + R.mipmap.weiyan_info,
                picture_src_top + R.mipmap.zhangfei_info,
                picture_src_top + R.mipmap.zhaoyun_info,
                picture_src_top + R.mipmap.zhugeliang_info
        };

        for ( int i=0 ; i<shu_picture.length ; i++ )
            shu_persons[i] = new Person("蜀",shu_name[i],shu_sex[i],
                    shu_place[i],shu_birth[i],shu_info[i],
                    shu_picture[i],shu_picture_info[i]) ;

        // Wei
        String wei_name[] = context.getResources().getStringArray(R.array.Wei_name);
        String wei_sex[] = context.getResources().getStringArray(R.array.Wei_sex);
        String wei_place[] = context.getResources().getStringArray(R.array.Wei_place) ;
        String wei_birth[] = context.getResources().getStringArray(R.array.Wei_birth) ;
        String wei_info[] = context.getResources().getStringArray(R.array.Wei_info) ;
        String wei_picture[] ={
                picture_src_top + R.mipmap.caocao,
                picture_src_top + R.mipmap.caozhi,
                picture_src_top + R.mipmap.dengai,
                picture_src_top + R.mipmap.dianwei,
                picture_src_top + R.mipmap.guojia,
                picture_src_top + R.mipmap.simayi,
                picture_src_top + R.mipmap.wanglang,
                picture_src_top + R.mipmap.xiahoudun,
                picture_src_top + R.mipmap.xuchu,
                picture_src_top + R.mipmap.yangxiu,
                picture_src_top + R.mipmap.yujin,
                picture_src_top + R.mipmap.zhangliao
        };
        String wei_picture_info[] ={
                picture_src_top + R.mipmap.caocao_info,
                picture_src_top + R.mipmap.caozhi_info,
                picture_src_top + R.mipmap.dengai_info,
                picture_src_top + R.mipmap.dianwei_info,
                picture_src_top + R.mipmap.guojia_info,
                picture_src_top + R.mipmap.simayi_info,
                picture_src_top + R.mipmap.wanglang,
                picture_src_top + R.mipmap.xiahoudun_info,
                picture_src_top + R.mipmap.xuchu_info,
                picture_src_top + R.mipmap.yangxiu_info,
                picture_src_top + R.mipmap.yujin_info,
                picture_src_top + R.mipmap.zhangliao_info
        };
        for ( int i=0 ; i<wei_picture.length ; i++ ){
            wei_persons[i] = new Person("魏",wei_name[i],wei_sex[i],
                    wei_place[i],wei_birth[i],wei_info[i],
                    wei_picture[i],wei_picture_info[i]) ;
        }

        // Wu
        String wu_name[] = context.getResources().getStringArray(R.array.Wu_name);
        String wu_sex[] = context.getResources().getStringArray(R.array.Wu_sex);
        String wu_place[] = context.getResources().getStringArray(R.array.Wu_place) ;
        String wu_birth[] = context.getResources().getStringArray(R.array.Wu_birth) ;
        String wu_info[] = context.getResources().getStringArray(R.array.Wu_info) ;
        String wu_picture[] ={
                picture_src_top + R.mipmap.daqiao,
                picture_src_top + R.mipmap.huanggai,
                picture_src_top + R.mipmap.lingtong,
                picture_src_top + R.mipmap.lusu,
                picture_src_top + R.mipmap.luxun,
                picture_src_top + R.mipmap.lvmeng,
                picture_src_top + R.mipmap.sunce,
                picture_src_top + R.mipmap.sunjian,
                picture_src_top + R.mipmap.sunquan,
                picture_src_top + R.mipmap.taishici,
                picture_src_top + R.mipmap.xiaoqiao,
                picture_src_top + R.mipmap.zhouyu,
                picture_src_top + R.mipmap.zhugejin
        };
        String wu_picture_info[] ={
                picture_src_top + R.mipmap.daqiao_info,
                picture_src_top + R.mipmap.huanggai_info,
                picture_src_top + R.mipmap.lingtong_info,
                picture_src_top + R.mipmap.lusu_info,
                picture_src_top + R.mipmap.luxun_info,
                picture_src_top + R.mipmap.lvmeng_info,
                picture_src_top + R.mipmap.sunce_info,
                picture_src_top + R.mipmap.sunjian_info,
                picture_src_top + R.mipmap.sunquan_info,
                picture_src_top + R.mipmap.taishici,
                picture_src_top + R.mipmap.xiaoqiao_info,
                picture_src_top + R.mipmap.zhouyu_info,
                picture_src_top + R.mipmap.zhugejin_info
        };
        for ( int i=0 ; i<wu_picture.length ; i++ )
            wu_persons[i] = new Person("吴",wu_name[i],wu_sex[i],
                    wu_place[i],wu_birth[i],wu_info[i],
                    wu_picture[i],wu_picture_info[i]) ;


    }
}