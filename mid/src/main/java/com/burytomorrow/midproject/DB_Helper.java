package com.burytomorrow.midproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by CJT on 2017/11/12.
 */

public class DB_Helper extends SQLiteOpenHelper{

    private Context context ;

    //数据库版本
    private static final int DB_VERSION = 1 ;

    //数据库名称
    private static final String DB_NAME="Three_Kingdoms.db";

    // 构造函数
    public DB_Helper(Context context){
        super(context,DB_NAME,null,DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // 创建数据表
        String Create_Table =
                "CREATE TABLE Person("
                        +"id INTEGER PRIMARY KEY AUTOINCREMENT,"
                        +"nation TEXT,"
                        +"name TEXT,"
                        +"sex TEXT,"
                        +"place TEXT,"
                        +"birth TEXT,"
                        +"info TEXT,"
                        +"picture TEXT,"
                        +"picture_info TEXT)" ;
        db.execSQL(Create_Table);

        // 插入数据
        Data data = new Data(context);
        // Shu
        ContentValues shu_value = new ContentValues() ;
        for ( int i=0 ; i<13 ; i++ ){
            shu_value.put("nation",data.shu_persons[i].nation);
            shu_value.put("name",data.shu_persons[i].name);
            shu_value.put("sex",data.shu_persons[i].sex);
            shu_value.put("place",data.shu_persons[i].place);
            shu_value.put("birth",data.shu_persons[i].birth);
            shu_value.put("info",data.shu_persons[i].info);
            shu_value.put("picture",data.shu_persons[i].picture);
            shu_value.put("picture_info",data.shu_persons[i].picture_info);
            db.insert("Person",null,shu_value) ;
        }
        // Wei
        ContentValues wei_value = new ContentValues() ;
        for ( int i=0 ; i<12 ; i++ ){
            wei_value.put("nation",data.wei_persons[i].nation);
            wei_value.put("name",data.wei_persons[i].name);
            wei_value.put("sex",data.wei_persons[i].sex);
            wei_value.put("place",data.wei_persons[i].place);
            wei_value.put("birth",data.wei_persons[i].birth);
            wei_value.put("info",data.wei_persons[i].info);
            wei_value.put("picture",data.wei_persons[i].picture);
            wei_value.put("picture_info",data.wei_persons[i].picture_info);
            db.insert("Person",null,wei_value) ;
        }
        // Wu
        ContentValues wu_value = new ContentValues() ;
        for ( int i=0 ; i<13 ; i++ ){
            wu_value.put("nation",data.wu_persons[i].nation);
            wu_value.put("name",data.wu_persons[i].name);
            wu_value.put("sex",data.wu_persons[i].sex);
            wu_value.put("place",data.wu_persons[i].place);
            wu_value.put("birth",data.wu_persons[i].birth);
            wu_value.put("info",data.wu_persons[i].info);
            wu_value.put("picture",data.wu_persons[i].picture);
            wu_value.put("picture_info",data.wu_persons[i].picture_info);
            db.insert("Person",null,wu_value) ;
        }



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}