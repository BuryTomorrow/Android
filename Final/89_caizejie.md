

</br>

</br>

<center><font style=font-size:32px>**Android移动应用开发期末项目报告** </font></center>

<center><font style=font-size:20px>**（2017年秋季学期）**</font></center>

</br>

<center><font style=font-size:40px>**笺**</font><font style=font-size:20px>**----便签**</font></center>

</br>

<div><div style="float:left;">**课程名称:移动应用开发**</div><div style="float:right">**任课老师:郑贵锋**</div><div style="clear: both;"></div></div>

------

|        |      学号      |   姓名    |            邮箱            |       QQ       |
| :----: | :----------: | :-----: | :----------------------: | :------------: |
| **组长** | **15352013** | **蔡泽杰** | **qchong7810874@qq.com** | **1095595077** |

------

### **1. 概述**

- 使用Android Studio编写一个具有富文本编辑功能的日常便签记录APP，同时便签的记录能够上传至云端进行保存，避免丢失。

### **2. 背景**

- 日常生活中我们经常会遇到各种各样需要进行记录的事，无论大小，都会使用到便签来做记录。可能某天看到美景，想要抒发下心情，而光有言语却不能完全表达，可能需要加入图片才能更好地表达，所以，我们使用一种富文本的便签作为一个记录用的APP，允许用户能够在编辑的文本当中加入图片、粗体字、斜体字、中划线等操作。
- 在使用便签过程当中，有时候只是想记录下自己愤懑的心情，这些心情是不希望被别人看见，所以我们提供了对编写的文章加密的功能，这么一来就可以随用户需要对必要的文章进行加密。
- 同时，日常使用中可能会发生以外的情况导致APP损坏，应用丢失数据或者误删的情况出现，所以我们提供了云存储的功能，按照用户需要，可以将本地的数据上传到服务器数据库进行保管，当需要的时候可以从服务器数据库上拉取原有保管的信息。
- 注意，在日常使用的时候对数据库的操作都是本地数据库的操作，而不是远程数据库操作，远程数据库可以看做是用户的一个冗余备份，来给予用户恢复丢失数据的方便。而不直接使用远程数据库方便了用户日常的使用，因为用户使用的时候可能不在联网的状态，比如到一个荒山野岭旅游，而那里又没网，风景却又很好，想做记录却发现因为连不上网而不给做记录岂不是很尴尬。

<div STYLE="page-break-after: always;"></div>

### **3. 个人负责部分说明**

- 我主要负责的是主界面的各类功能编写以及对服务器数据库的操作功能，其中主界面功能包括主界面各个按钮的操作、视图效果、视图动画、日期筛选传回结果后的数据库筛选操作。
- 实现的是本地数据库以及云数据库两个操作，一般情况下使用本地数据库进行数据库操作，主要是为了方便不联网的情况下同样能够使用APP。而通过用户操作，当用户选择"同步到云"或者"从云拉取"的时候就会访问远程的云数据库，而那时候就是同步两个数据库。

#### **3.1. 逻辑流程**

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\流程图.png" height = 520></center>

<div STYLE="page-break-after: always;"></div>

#### **3.2. 功能模块、应用效果及技术说明**

##### 3.2.1. 视图切换

实现视图效果如下：

|                   列表视图                   |                   宫格视图                   |
| :--------------------------------------: | :--------------------------------------: |
| <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\列表视图.png" height = 300> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\宫格视图.png" height = 300> |

通过点击右上角的视图切换按钮来进行视图的切换操作：每次切换的时候都重新初始化使用的RecyclerView，因为不同视图使用的列表布局不同：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.1-1.png"></center>

其中mode作为全局变量，是`bollean`值，每次点击都会进行切换然后调用该初始化函数重新进行视图布局。

##### 3.2.2. 设置按钮操作

当点击设置按钮的时候会在设置按钮的下方弹出一个小的下拉窗口，同样也是使用$popupwindow$的方式，具体的操作在$3.2.4$特殊模式中细说相关设置，不过与特殊模式不同的是这里使用的是另一个简单的布局$layout$，只有三个按钮，同时通过偏移设置其显示位置正好在设置按钮的正下方。效果如下：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\设置按钮弹窗.png" height = 200></center>

其中有三个选项：

- **日期筛选：** 跳转到日期筛选的界面，通过日期筛选后传回的内容重新更新视图，其中在`onActivityResult`函数当中对筛选结果的回应操作如下：主要是筛选出选中的对应日期的文章并将其加入到显示的列表当中，再初始化显示视图就可以了：

  ```java
  SQLiteDatabase db  = db_helper.getReadableDatabase() ;
  Cursor cursor = db.rawQuery("SELECT * FROM Article", null);
  List<Article> temp_article = new ArrayList<>();//作为临时缓存
  if (cursor.moveToFirst()) {
    do {temp_article.add(new Article(/*这里添加原数据库当中全部信息*/));} 
    while (cursor.moveToNext());
  }
  articles.clear();//清空需要显示的视图列表内容
  ArrayList<CharSequence>YEAR = data.getExtras().getCharSequenceArrayList("year");
  for(int i=0;i<YEAR.size();i++){//获取筛选后的年份结果
    CharSequence temp = YEAR.get(i);
    int a = Integer.parseInt((String) YEAR.get(i));
    ArrayList<CharSequence>MONTH = data.getExtras().getCharSequenceArrayList(temp.toString());
    for(int j=0;j<MONTH.size();j++){//获取每一年中选中的月份
      int b = Integer.parseInt((String) MONTH.get(j));
      for(int k=0;k<temp_article.size();k++){//遍历原数据库中所有数据
        if(temp_article.get(k).year == a&&temp_article.get(k).month==b){
          articles.add(temp_article.get(k));//将选中的数据加入到需要显示的队列
        }
   }}}
  initRecyclerView();//重新初始化需要显示的视图
  ```

- **同步到云：** 

  当用户选择将本地数据库的数据同步到云的时候，会启动一个新的Acitivity来对其进行操作，不过改Activity的声明周期很短，可以看做只是负责远程数据库操作的一个隐藏界面，在进行数据库同步操作的时候，应当避免用户此时会操作本地数据库，所以会将列表视图更改为显示进度条，当数据库同步完成后进度条才会变更回列表，具体在$3.2.3$节解释。 

- **从云拉取：** 

  同样是对本地数据库和远程数据库的同步操作，与"同步到云"操作不同的是会先将本地的数据库清空然后替换为远程数据库的内容。具体在$3.2.3$节解释。 

##### 3.2.3. 远程数据库操作(EventsBus+Thread+jdbc)

远程数据库操作结果：

|              远程数据库操作时显示进度条               |              连接数据库失败显示错误信息               |            成功将本地数据库更新为远程数据库内容            |             成功将本地数据库上传到远程数据库             |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\进度条.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\同步失败.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\拉取成功.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\同步成功.png" height = 200> |

需要注意的是远程数据库$jdbc$访问需要是异步访问的，那么需要在异步访问完成后通知主界面完成了并更新列表视图，所以需要使用到$EventBus$来进行处理，所以主机面需要注册$EventBus$: 

然后关于跳转到隐藏的远程数据库操作界面：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-1.png"></center>

跳转的时候需要传递参数高数隐藏界面是"同步到云"还是"从云拉取"的操作，然后对于隐藏界面，主要是启动一个异步线程访问远程数据库，然后即立马技术该界面回到主界面，所以在视觉效果上不会看到视图的切换，视图会一直保留在主界面上：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-2.png"></center>

而启动的异步线程当中主要是对数据库的操作，首先需要连接到远程数据库：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-5.png"></center>

注意到远程数据库的访问是需要加入新的$jar$包的，这里在$build.gradle$的$android$函数中加入：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-6.png"></center>

以及在$defaultConfig$函数当中加入：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-7.png"></center>

之后再加入$jar$包到项目文件目录$appl/ibs$下，这里我们使用的是$mysql-connector-java-5.1.39-bin.jar$ 。

完成之后再进行远程数据库的操作：

- **同步到云：**

  同步到远程数据库需要避免往远处数据库当中插入多余重复信息，所以需要先清空远程数据库的内容，通过使用`where  1 = 1`来清空表内全部元组，然后再将数据全部插入到远程数据库当中，当插入完成后通过使用$EventBus$来通知主界面更新。

  (其中出现共色错误信息是因为为了展示而去除相似的重复步骤，避免篇幅过长)

  <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-3.png">

- **从云拉取：**

  从远程数据库中获取数据内容，需要先清空本地数据库的内容，同样可以通过`where 1 = 1`来将所有的元组全部删除，然后通过访问数据库，从数据库当中获取全部数据并插入到本地数据库当中，最后同样通过$EventBus$通知主界面，因为是更新本地数据库了，所以主界面当然也就需要对列表视图进行更新，避免数据不一致的情况。

  <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-4.png">

当数据库访问失败的时候同样也会通过$EventBus$来通知主界面，只要通过传递的参数就可以知道隐藏界面的工作，主界面通过`@Subscribe(threadMode = ThreadMode.MAIN)`和函数`public void onMessageEvent(String a)`来对接受到的消息进行处理：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.3-8.png"></center>

##### 3.2.4. 特殊模式(MD5加密)

|                 选择多项进行加密                 |                  加密成功后                   |                  点击加密项                   |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\多选加密.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\加密后.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\点击加密项.png" height = 200> |

|                加密项必须单独操作                 |                全选操作(列表模式)                |                    删除                    |
| :--------------------------------------: | :--------------------------------------: | :--------------------------------------: |
| <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\加密单独操作.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\全选.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\删除.png" height = 200> |

长按列表中任意项之后可以进入特殊模式，在该模式下搜索输入框会消失，同时上方的标题变更为选择项的个数统计，以及左右两边的两个控件变更为"取消"按钮以及"全选"按钮，其中全选按钮按下的时候会选择所有的项同时按钮变更为"全不选"。

在该特殊模式下，列表中所有的项的右下角都会添加上一个勾选框(列表视图下是左上角)，然后点击任意项会勾选，点击加密按钮的时候弹出`popupWindow`(下拉窗)，以动画的形式从上往下弹出，其中代码如下：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.4-1.png"></center>

该函数是可以重复调用的，也可以递归调用，所以通过传参来完成同一个布局下的不同显示，如图中显示的是"输入新密码"的弹窗以及"输入密码"的弹窗。同时因为可以对以及加密的子项进行更改密码，所以会使用到`popupWindow`的递归调用，在弹出第一个窗口输入正确密码之后会弹出第二个窗口供输入新密码同时让第一个窗口消失，代码流程大致如下：
$$
\begin{align}
&点击OK按钮：\\
&\ \ \ \ if \ \ mode = 修改密码:\\
&\ \ \ \ | \ \ \ \ 密码相同？\ 递归调用popUpWindow输入新密码:显示错误\\
&\ \ \ \ if \ \ mode = 设密码:\\
&\ \ \ \ | \ \ \ \ 判断输入密码是否为空and前后输入密码是否相同,相同则使用MD5Utils加密\\
&\ \ \ \ if \ \ mode = 跳转\\
&\ \ \ \ | \ \ \ \ 密码相同？\ 跳转:输出错误信息\\
&\ \ \ \ if \ \ mode = 删除\\
&\ \ \ \ | \ \ \ \ 密码相同？删除:输出错误信息
\end{align}
$$
注意在设密码和删除操作的时候，如果选中项包含了已经设置密码的项，那么就需要对设密码的项进行单独操作，因为对设密码项的任何操作都应该需要输入密码才能进行操作。

注意到加密的时候是使用$MD5Utils$进行加密的，可以通过编写类$MD5Utils$来进行相关操作：

```java
public static String md5Password(String password){
  StringBuffer sb = new StringBuffer();
  try {
    MessageDigest digest = MessageDigest.getInstance("md5");
    byte[] result = digest.digest(password.getBytes());
    //加密运算，对密码的每一个byte做与运算0xff
    for(byte b:result){
      int number = b & 0xff;
      String str = Integer.toHexString(number);
      if(str.length()==1){sb.append("0");}
      sb.append(str);
    }
  }catch(NoSuchAlgorithmException e){
    e.printStackTrace();
  }
  return sb.toString();
}
```

然后在调用的时候使用：

```java
MD5Utils.md5Password(inputPW)
```

就可以对输入的字符串进行加密了，对于密码的加密就只需要单向加密，也就是说不需要进行解密，当需要判断输入密码是否正确的时候只需要判断存储的已经经过$MD5Utils$密码是否和新输入的密码进行$MD5Utils$加密后的字符串相等就可以确定密码是否正确。其中加密后效果示例如下：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\MD5加密.png" height = 100></center>

最后关于删除的操作，基本操作流程和设密码一样，同样对已加密的项必须单独操作，同时加密项必须得确保输入正确密码才能够执行删除操作，删除操作针对的是本地数据库的删除(不是对远程数据库的操作，如果想对远程数据库进行操作，则可以在删除本地数据库内容后再执行"同步到云"操作来更改远程数据库。当然，如果不小心删除了不像删除的东西，也就可以通过执行"从云拉取"操作来进行数据的恢复，相当于提供了冗余可靠性)。而对于没有加密的项则可以直接多选删除，删除的时候会有弹出框询问是否删除并显示总共删除的项目数量。

##### 3.2.5. 搜索框

搜索框的操作是通过一个子线程去监听输入的数据进行搜索，也就是说不需要点击搜索按钮就能够完成搜索的功能：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\搜索.png" height = 300></center>

首先是建立一个子线程：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.5-1.png"></center>

该子线程会一直在运行，每100ms发送一个消息给$Handler$进行处理：

<center><img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.5-2.png"></center>

在$Handler$处理中会一直判断输入框的内容，如果检测到输入框的内容发生了变化，那么就调用函数`reInitData`函数对视图进行更新，也就是重新从本地数据库当中筛选数据然后显示相关的信息，其中查找使用的内容的相关查找方式，也就是SQL语句：

<img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.5-3.png">

##### 3.2.6. 列表滑动监听

有时候，列表的内容会过多，这个时候当用户滑到底部的时候又想返回到上方就会感觉很麻烦，所以添加一个返回至列表顶部的悬浮按钮，同时，为了避免界面已经显示到了最上方以及列表项不足一页的情况下该按钮占用空间以及视觉上的违和感，设定只有当界面没有显示最上方项的时候才会显现出返回顶部的按钮：

|             当界面中已经显示最上方项，则隐藏             |             当界面中没有显示最上方项，则显示             |
| :--------------------------------------: | :--------------------------------------: |
| <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\顶部.png" height = 200> | <img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\底部.png" height = 200> |

实现的方式是通过监听`RecyclerView`的监听事件来完成，同时使用`RecyclerView.LayoutManager`来获取屏幕内显示的$items$相关信息：

<img src="E:\HomeWork\Grade_3_Last\Android_Studio\Final\期末报告\代码图\3.2.6-1.png">

通过列表滑动监听，当列表滑动的时候就检测屏幕内$items$总数，以及最后一个$item$在整个列表当中的$position$，显然，如果屏幕显示的不是列表的最上方，那么最后一个$item$的$position$就一定会大于整个屏幕内总的$items$数量，这样就可以判断出要不要显示返回顶部的按钮了。

### **4. 开发过程中遇到的问题以及解决方案**

- **如何连上远程数据库并进行操作：**

  一开始试着使用Web所学的方式进行远程数据库的连接，通过添加$jar$包后程序运行会报错，原因是版本的问题，需要修改$build.gradle$的相关设置。当程序能够运行起来的时候发现总是不能连上数据库，才发现关于远程数据库$jdbc$的连接方式必须得使用异步的方式才能够连接，因为主线程是不允许过于耗时的操作的，所以不能直接使用主线程去访问远程数据库。这个时候就可以通过创建一个子线程来访问远程的数据库了。

  但是随之而来的就是怎么才能够确定我连接上了数据库并进行视图的更新呢？而这个的解决办法就比较好想了，因为我们学过$EventBus$，那么就可以通过把远程数据库的操作放到另一个$Activity$当中，这个$Activity$是相当于一个隐藏的界面，然后主界面通过监听隐藏的界面发送过来的$EventBus$来确定异步线程是否完成了远程数据库的连接以及相关操作，即便连接失败也能够提示相应的错误。

- **如何不使用搜索按钮就能够进行搜索：**

  因为在界面中加入搜索按钮其实挺影响美观的，如果能够去掉搜索的按钮就有助于提高界面的美观程度，而随之能够想到的就是学过的创建$Thread$，通过子线程来每隔一段时间获取输入框的内容，然后对比上一次监听输入框的输入内容得到的输入，判断是否一致，如果不一致就意味着用户输入了内容，这个时候就可以进行筛选操作更新视图了。

- **如何在滑动到下方的时候才显示返回顶部的按钮：**

  这个同样困惑了挺久的，如果单单只是使用滑动监听的话，我只能知道滑动的距离，但是我又不能通过每次的滑动时间观察他滑动了多少距离然后来显示按钮，这显然是不合理的。所以能想到的就是在发生滑动的时候确定视图中$items$的相对位置，通过不断查找资料发现其实`RecyclerView.LayoutManager`当中是可以监控到屏幕内$item$的位置的，同时还能够针对是线性视图还是宫格视图设置不同的`LayoutManager`模式来获取相关的位置，在这里实际上最主要的就是获取整个屏幕内总的$items$的数量(是屏幕内显示的不是真个列表拥有的总数)，以及显示的最后一项的位置就可以判断需不需要显示返回顶部的按钮了。

### **5. 项目总结**

- 说实话这次实验收获很大。查找了非常多的资料，尽可能地用上自己所学过的知识，不仅仅只是用$Android$课上的知识同时还用上了$Web$课程所学的内容来完成这次$Project$，同时不断地查找新的知识，扩展自己的知识，将新知识和学过的知识统合起来建立一个功能较为完备的APP，虽然很累但很值得，也很有趣。虽然可能界面还不够美观，但用户相关的操作基本上都符合市场上的APP。
- 同时也体会到了后端设计和前端设计分开设计的好处，因为在$Web$课程的设计当中的$JSP$是前端后端都写在一起，感觉就很难受，而安卓设计好前端后就进行后端的逻辑设计，有什么不满意的视觉就直接在前端里继续改改就好，就舒服很多。虽然上面的代码当中没有体现出前端设计时的辛苦，但实际上前端设计的时候也花费了不少心力，毕竟想要动画效果以及各种比较好看的相对布局，所以同样需要花费不少时间(找图也花好多时间啊)。

### **6. 课程总结** 

- 首先，感动的是没有期末考试，因为没有复习周真的学不来了。
- 然后因为没有期末考试，所以每次实验都是认真去完成的，尽可能在学会要求掌握的内容的同时去扩展新的知识，为两个大项目做准备。然后在期中项目的时候其实就已经为期末项目打好了基础了，期中的项目其实也就差在了联网这种功能，因为当时还是比较一脸懵的，所以也就只能做到一个单机版了。不过在期中项目里就已经运用了不少课上没有学到的内容了。
- 而到了期末，就继续拓展自己学过的知识，其实主要就是想做得能够和市面上存在的APP类似的功能，毕竟人家能够做到，那么就一定有相关的知识能够学到，所以就不断地查找资料，已经用实验课上学过的各种内容去解决各类BUG，虽然挺多时候查找网上的内容都得不到自己想要的方法，但是可以通过各类方法都学一下然后统一起来就可以得到自己想要的效果比如最后的那个滑动监听，在网上找的基本都不是我所使用的这类方法。而对于数据库的操作，同样也是结合了网上的以及自己的想法才完成的。
- 总的来说能选到这门课感觉挺开心的，因为确实能够学到不少的东西，而且很使用。