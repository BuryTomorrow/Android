package com.burytomorrow.myapplication;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private ImageView myimage;
    private RadioGroup radioGroup;
    private RadioButton radioB1;
    private RadioButton radioB2;
    private Button login;
    private Button register;
    private EditText ed1;
    private EditText ed2;
    private TextInputLayout username;
    private TextInputLayout password;
    AlertDialog.Builder builder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        dialog();
        Listener();
    }
    void init(){
        myimage=(ImageView) findViewById(R.id.imageView);
        radioGroup=(RadioGroup) findViewById(R.id.id0);
        radioB1=(RadioButton) findViewById(R.id.radioButton3);
        radioB2=(RadioButton) findViewById(R.id.radioButton4);
        login=(Button) findViewById(R.id.button);
        register=(Button) findViewById(R.id.button2);
        ed1=(EditText) findViewById(R.id.editText);
        ed2=(EditText) findViewById(R.id.editText2);
        username=(TextInputLayout) findViewById(R.id.textInputLayout);
        password=(TextInputLayout) findViewById(R.id.textInputLayout2);
    }
    void dialog(){ //初始化对话框内容：
        final String[] items=new String[]{"拍摄","从相册中选择"};
        builder=new AlertDialog.Builder(this);
        builder.setTitle("上传头像");
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(MainActivity.this,"您选择了【取消】",Toast.LENGTH_SHORT).show();
            }
        });
        builder.setItems(items,new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(items[which].equals("拍摄")){
                    take_photo();
                }else if(items[which].equals("从相册中选择")){
                    select_photo();
                }
                Toast.makeText(MainActivity.this,"您选择了"+items[which],Toast.LENGTH_SHORT).show();
            }
        });
    }
    void Listener() {
        myimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.create().show();
            }
        });
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if(radioB1.isChecked()){
                    Snack("您选择了学生");
                }else if(radioB2.isChecked()){
                    Snack("您选择了教职工");
                }
            }
        });
        login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(TextUtils.isEmpty(ed1.getText())){
                    username.setError("学号不能为空");
                    password.setErrorEnabled(false);
                }else if(TextUtils.isEmpty(ed2.getText())){
                    username.setErrorEnabled(false);
                    password.setError("密码不能为空");
                }else{
                    username.setErrorEnabled(false);
                    password.setErrorEnabled(false);
                    if(ed1.getText().toString().equals("123456") && ed2.getText().toString().equals("6666")){
                        Snack("登录成功");
                    }else{
                        Snack("学号或密码错误");
                    }
                }
            }
        });
        register.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if (radioB1.isChecked()) {
                    Snack("学生注册功能尚未启用");
                } else if (radioB2.isChecked()) {
                    Toast.makeText(MainActivity.this,"教职工注册功能尚未启用",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    void Snack(String s){
        Snackbar.make(radioGroup, s, Snackbar.LENGTH_LONG).setAction("确定", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Snackbar的确定按钮被点击了", Toast.LENGTH_SHORT).show();
            }
        }).setActionTextColor(getResources().getColor(R.color.Blue)).show();
    }
    void select_photo(){
        Intent intent=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent,1);
    }
    void take_photo(){
        Intent intent=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent,2);
    }
    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        if(requestCode==1){
            Uri uri=data.getData();
            myimage.setImageURI(uri);
        }else if (requestCode==2){
            Bundle bundle=data.getExtras();
            Bitmap bitmap=(Bitmap) bundle.get("data");
            myimage.setImageBitmap(bitmap);
        }
    }
}

