package com.burytomorrow.lab_7;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by BuryTomorrow on 2017/12/10.
 */

public class File_activity extends AppCompatActivity {
    private TextView title;
    private EditText fileName;
    private EditText file;
    private Button save;
    private Button load;
    private Button clear;
    private Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.file_activity);

        init();
        listener();
    }
    public void init(){
        title = (TextView) findViewById(R.id.mtitle);
        fileName = (EditText) findViewById(R.id.file_name);
        file = (EditText) findViewById(R.id.file);
        save = (Button) findViewById(R.id.save);
        load = (Button) findViewById(R.id.load);
        clear = (Button) findViewById(R.id.f_clear);
        delete = (Button) findViewById(R.id.delete);
    }

    public void listener(){
        //去掉输入法
        title.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                title.setFocusable(true);
                title.setFocusableInTouchMode(true);
                title.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(title.getWindowToken(), 0);
                return false;
            }
        });
        //保存按钮
        save.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                String name = fileName.getText().toString();
                String text = file.getText().toString();
                try (FileOutputStream fileOutputStream = openFileOutput(name,MODE_PRIVATE)){
                    fileOutputStream.write(text.getBytes());
                    Toast.makeText(File_activity.this,"保存成功",Toast.LENGTH_SHORT).show();
                }catch (IOException e) {
                    Toast.makeText(File_activity.this,"保存出错，文件名不能为空",Toast.LENGTH_SHORT).show();
                }
            }
        });
        //加载按钮
        load.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                String name = fileName.getText().toString();
                try(FileInputStream fileInputStream = openFileInput(name)){
                    byte[] contents = new byte[fileInputStream.available()];
                    String s=new String(contents,0,fileInputStream.read(contents));
                    file.setText(s);
                    Toast.makeText(File_activity.this,"文件读取成功",Toast.LENGTH_SHORT).show();
                }catch (IOException e){
                    file.setText("");
                    Toast.makeText(File_activity.this,"不存在该文件，加载失败",Toast.LENGTH_SHORT).show();
                }
            }
        });
//        //清空按钮
        clear.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                file.setText("");
            }
        });
        //删除按钮
        delete.setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v){
                String name = fileName.getText().toString();
                if(deleteFile(name)){
                    file.setText("");
                    Toast.makeText(File_activity.this,"文件删除成功",Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(File_activity.this,"文件删除失败",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
//
//    @Override
//    public void onBackPressed() {
//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        startActivity(intent);
//    }
}
