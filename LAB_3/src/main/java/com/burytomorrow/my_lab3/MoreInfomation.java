package com.burytomorrow.my_lab3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by BuryTomorrow on 2017/10/22.
 */
//商品详细信息界面
public class MoreInfomation extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infomation);

        final Info p=(Info) getIntent().getSerializableExtra("Info");
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.Top);

        //返回按钮
        Button back=(Button) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
        //更新图片
        ImageView image = (ImageView) findViewById(R.id.shopimage);
        image.setImageResource(p.getImage());
        //商品名字
        TextView Name = (TextView) findViewById(R.id.Name);
        Name.setText(p.getName());
        //商品价格
        TextView price = (TextView) findViewById(R.id.price);
        price.setText(p.getPrice());
        //商品类型
        TextView type = (TextView) findViewById(R.id.type);
        type.setText(p.getType());
        //产品信息
        TextView type_info = (TextView) findViewById(R.id.type_info);
        type_info.setText(p.getGoods_info());

        //更多详细信息列表
        String[] operation = new String[]{"假的一键下单","才不分享商品","真的不感兴趣","不存在查看更多商品促销信息"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,R.layout.more,operation);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(arrayAdapter);

        //星星切换
        final Button star= (Button) findViewById(R.id.star);
        star.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(!tag){
                    star.setBackgroundResource(R.mipmap.full_star);
                    tag=true;
                }else{
                    star.setBackgroundResource(R.mipmap.empty_star);
                    tag=false;
                }
            }
        });

        //点击购买
        final ImageButton buy = (ImageButton) findViewById(R.id.buy);
        buy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(MoreInfomation.this,"商品已添加到购物车",Toast.LENGTH_SHORT).show();
                Intent mIntent = new Intent();
                mIntent.putExtra("Info",p);
                setResult(1,mIntent);
            }
        });
    }
    private boolean tag = false;
}
