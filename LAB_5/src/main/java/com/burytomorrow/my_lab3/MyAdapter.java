package com.burytomorrow.my_lab3;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by BuryTomorrow on 2017/10/22.
 */

public class MyAdapter extends BaseAdapter{
    private Context context;
    private List<Info> list;

    public MyAdapter(Context context,List<Info> list){
        this.context=context;
        this.list = list;
    }

    @Override
    public int getCount(){
        if(list == null){
            return 0;
        }
        return list.size();
    }

    @Override
    public Object getItem(int i){
        if(list == null){
            return null;
        }
        return list.get(i);
    }
    @Override
    public long getItemId(int i){
        return i;
    }

    @Override
    public View getView(int i, View view , ViewGroup viewGroup){
        //新声明一个View变量和ViewHolder变量
        View convertView;
        ViewHolder viewHolder;

        //当view为空时才加载布局，并且创建一个ViewHolder，获得布局中的两个控件
        if(view == null){
            //通过inflate的方法加载布局，context这个参数需要使用这个adapter的Activity传入
            convertView = LayoutInflater.from(context).inflate(R.layout.shopping_list,null);
            viewHolder = new ViewHolder();
            viewHolder.cycle = (TextView) convertView.findViewById(R.id.cycle);
            viewHolder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(viewHolder);//用setTag方法将处理好的viewHolder放入view中
        }else{ //否则让convertView等于view，然后从中去除ViewHolder即可
            convertView = view;
            viewHolder = (ViewHolder) convertView.getTag();
        }
        //从viewHolder中取出对应的对象，然后赋值给它们
        viewHolder.cycle.setText(list.get(i).getcycle());
        viewHolder.name.setText(list.get(i).getName());
        //将处理好的view返回
        return convertView;
    }

    private class ViewHolder{
        public TextView cycle;
        public TextView name;
    }
}
