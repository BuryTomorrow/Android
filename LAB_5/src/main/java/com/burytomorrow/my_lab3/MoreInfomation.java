package com.burytomorrow.my_lab3;

import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by BuryTomorrow on 2017/10/22.
 */
//商品详细信息界面
public class MoreInfomation extends AppCompatActivity{
    private static final String DYNAMICACTION = "com.example.ex4.MyDynamicFliter";//动态广播
    //BUG修复：
    @Override
    protected void onNewIntent(Intent intent){
        p= (Info) intent.getSerializableExtra("Info");
        main_work();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infomation);
        p=(Info) getIntent().getSerializableExtra("Info");
        main_work();
    }
    //主要配置工作
    public void main_work(){
        //注册动态广播代码：
        IntentFilter dynamic_filter = new IntentFilter();
        dynamic_filter.addAction(DYNAMICACTION);//添加动态广播的Action
        registerReceiver(new mReceiver(),dynamic_filter);//注册自定义动态广播消息
        registerReceiver(new mWidget(),dynamic_filter);
        //返回按钮
        Button back=(Button) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finish();
            }
        });
        //更新图片
        ImageView image = (ImageView) findViewById(R.id.shopimage);
        image.setImageResource(p.getImage());
        //商品名字
        TextView Name = (TextView) findViewById(R.id.Name);
        Name.setText(p.getName());
        //商品价格
        TextView price = (TextView) findViewById(R.id.price);
        price.setText(p.getPrice());
        //商品类型
        TextView type = (TextView) findViewById(R.id.type);
        type.setText(p.getType());
        //产品信息
        TextView type_info = (TextView) findViewById(R.id.type_info);
        type_info.setText(p.getGoods_info());

        //更多详细信息列表
        String[] operation = new String[]{"假的一键下单","才不分享商品","真的不感兴趣","不存在查看更多商品促销信息"};
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this,R.layout.more,operation);
        ListView listView = (ListView) findViewById(R.id.listview);
        listView.setAdapter(arrayAdapter);

        //星星切换
        final Button star= (Button) findViewById(R.id.star);
        star.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(!tag){
                    star.setBackgroundResource(R.mipmap.full_star);
                    tag=true;
                }else{
                    star.setBackgroundResource(R.mipmap.empty_star);
                    tag=false;
                }
            }
        });

        //点击购买
        final ImageButton buy = (ImageButton) findViewById(R.id.buy);
        buy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(MoreInfomation.this,"商品已添加到购物车",Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post(p);

                Intent intentBroadcast = new Intent("com.example.ex4.MyDynamicFliter");
                intentBroadcast.putExtra("Info",p);
                sendBroadcast(intentBroadcast);
            }
        });
    }

    private boolean tag = false;
    private Info p;
}
