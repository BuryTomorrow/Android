package com.burytomorrow.mylab8;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by BuryTomorrow on 2017/12/18.
 */

public class AddActivity extends AppCompatActivity {
    public Button add;
    public EditText add_name;
    public EditText add_birth;
    public EditText add_gift;
    private static final String TABLE_NAME = "MyTable";

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_info);

        init();
        listener();
    }
    public void init(){
        add = (Button) findViewById(R.id.add_check);
        add_name = (EditText) findViewById(R.id.add_name);
        add_birth = (EditText) findViewById(R.id.add_birth);
        add_gift = (EditText) findViewById(R.id.add_gift);
    }
    public void listener(){
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //判断不能为空
                if(add_name.length()==0){
                    Toast.makeText(AddActivity.this,"名字不能为空",Toast.LENGTH_SHORT).show();
                }else{
                    String name = add_name.getText().toString();
                    String birth = add_birth.getText().toString();
                    String gift = add_gift.getText().toString();

                    mySQLite db = new mySQLite(getBaseContext());
                    SQLiteDatabase sqLiteDatabase = db.getWritableDatabase();
                    Cursor cursor = sqLiteDatabase.rawQuery("select * from "+ TABLE_NAME + " where name = '" + name +"'",null);
                    if(cursor.moveToFirst()==true){//如果名字已经存在
                        Toast.makeText(AddActivity.this,"名字重复了，请检查",Toast.LENGTH_SHORT).show();
                    }else{
                        //加入数据库
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("name",name);
                        contentValues.put("birth",birth);
                        contentValues.put("gift",gift);
                        sqLiteDatabase.insert(TABLE_NAME,null,contentValues);
                        sqLiteDatabase.close();
                        setResult(2,new Intent());
                        finish();
                    }
                }
            }
        });
    }
}
